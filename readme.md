# Description
MCQR is an R package dedicated to statistical analysis of proteomic data.
The goal of the MCQR is to provide a consistent set of functions facilitating proteomics data statistical analysis. It can be used without extensive programming knowledge with R.

# Help and support
MCQR is developped by the PAPPSO team. We are providing support through our [mailing list](http://pappso.inrae.fr/mailinglist/). Don't hesitate to register if you want to be informed of new releases or upgrades. The development is open and free, you are welcome to contribute, copy, modify, fork if you need it, respecting the terms of the [licence]({{< ref "#licence">}}).

# Download and install
All downloadable files concerning MCQR 1.0.2 can be found at [ForgeMIA](https://forgemia.inra.fr/pappso/mcqr/-/releases/MCQR_1.0.2).

## Under Windows 10  and later
To install this package, launch R (version >= 4) and execute these R commands :

```R
#load script in R
source("https://forgemia.inra.fr/-/project/1534/uploads/58c6f1c8b479ba9ff0b623cea63d01e5/script_install_41_1.0.2.R")

# check dependancies, install missing ones and install latest releases of MCQR
install_MCQR()
```


## Under Debian 11 or Ubuntu 22.04 or later

Install the following required packages in a terminal :
```bash
sudo apt update
sudo apt install r-base wget curl liblapack-dev libxml2-dev libblas-dev libcurl4-openssl-dev libnlopt-dev libgdal-dev libudunits2-dev libssl-dev
```

Launch R (version >= 4) and execute these R commands :

```R
#load script in R it will automatically install MCQR packages and dependancies
source("https://forgemia.inra.fr/-/project/1534/uploads/58c6f1c8b479ba9ff0b623cea63d01e5/script_install_41_1.0.2.R")

# check dependancies, install missing ones and install latest releases of MCQR
install_MCQR()
```

# Annotated script

MCQR comes with an annotated script available by executing following command in R
```R
#load MCQR
library(MCQR)
# write Annotated scripts in working directory
# you can specify the output directory by specify a valid directory for directory argument
mcq.write.scenarios(directory=".")
```
The manual and a full tutorial is available following those links :
* [Tutorial_for_the__MCQR__package_for_R.pdf](https://forgemia.inra.fr/-/project/1534/uploads/ad485644e06d343e7b99e8c7a853582e/Tutorial_for_the__MCQR__package_for_R.pdf)
* [MCQR-manual.pdf](https://forgemia.inra.fr/-/project/1534/uploads/e79026bcf696aa452f702b71b7316eb0/MCQR-manual.pdf)


# Licence
MCQR is a free software distributed under the terms of the [GNU GENERAL PUBLIC](http://www.gnu.org/licenses/gpl-3.0.html) licence v3.
