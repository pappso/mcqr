### Script d'installation de MCQR
## internal variable to modify depending on R version and current mcqr version
Rminrequirement <- "4.0.0"
packages =  c("car", "cpp11", "stringr", "readODS", "VIM", "ade4", "gplots", "clValid", "agricolae", "ggplot2", "RColorBrewer", "plotly", "plyr", "dplyr", "data.table", "nlme", "ggVennDiagram", "dataverse", "callr")

## internal function to ask for valdiation
## inspired from https://stackoverflow.com/questions/27112370/make-readline-wait-for-input-in-r

checkPackageAndInstallIt <- function(libraries, osName){
  result = TRUE
  installedPackages<- rownames(installed.packages())
  for(mylib in libraries){
    if(!(mylib %in% installedPackages)){
      cat(paste0("installing ", mylib, " Package\n"))
      install.packages(mylib, quiet=TRUE)
    }else{
        cat(paste0(mylib, " Package is already installed, skip it\n"))
    }
  }
  return(result)
}



install_MCQR <- function(prepare=FALSE){
  # Check environnment
  os <- Sys.info()
  cat("Operating System\n")
  cat(paste0(os['sysname'], " ", os['release'], " ", os['version'], "\n"))
  cat(paste0("Machine type : ", os['machine'], "\n"))

  ### checking R version for listing of necessary packages

  RVersion <- R.Version()
  cat(paste0(RVersion$version.string, "\n"))

  ## specific installation depending on R version
  if(as.integer(RVersion$major)<4){
    stop(cat(paste0("Sorry your R version is too old for the MCQR Package, the minimum requirement is R version",Rminrequirement, "\n")))
  }

  ## check and install needed packages
#   cat("for windows install prefer binary install instead of source install\n")
  test = checkPackageAndInstallIt(packages, os["sysname"])

  if(!prepare){
  #Install MCQR
    if("MCQR" %in% rownames(installed.packages())){
      cat("MCQR was already installed. Removing it\n")
      remove.packages("MCQR")
    }
    cat("Installing latest version of MCQR\n")
    install.packages('https://forgemia.inra.fr/pappso/mcqr/uploads/ee5588ea36d552e621b076fc050f1d01/MCQR_0.6.15.tar.gz', repos = NULL, type = 'source')
    }
  }

 install_MCQR(TRUE)
