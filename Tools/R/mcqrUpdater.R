update.mcqr <- function(path="."){
    olddir = getwd()
    setwd(path)
    system("R CMD build MCQR")
    
    if ("MCQR" %in% names(sessionInfo()$otherPkgs)){
      detach("package:MCQR", unload = TRUE)
    }

    packages <- installed.packages()
    MCQR_installed <- packages[packages[,1] == "MCQR",]
    if(length(MCQR_installed)!=0){
      if(length(MCQR_installed) == 16){
        libpath = MCQR_installed[[2]]
        cat(paste0("Removing MCQR from path ", libpath,"\n"))
        remove.packages("MCQR", lib= libpath)
      }else{
        for(i in range(1:dim(MCQR_installed)[1])){
          libpath = MCQR_installed[i,][[2]]
          cat(paste0("Removing MCQR from path ", libpath,"\n"))
          remove.packages("MCQR", lib= libpath)
        }
      }
    }
  descriptionFile <- file(description = "MCQR/DESCRIPTION", open="r", blocking = TRUE)
  version <- ""
  version_found <- FALSE
  line <- ""
  while(!version_found & length(line) !=""){
    line = readLines(descriptionFile, n = 1)
    if(grepl("Version: ", line)){
      version_found <- TRUE
      version <- sub("Version: ", "", line)
    }
  }
  close(descriptionFile)
  install_path = paste0("MCQR_", version,".tar.gz")

  cat(paste0("Installing MCQR version ", version, " from ", install_path, " file\n"))
  install.packages(install_path, repos = NULL, type = "source")
  library(MCQR)
  setwd(olddir)
}
