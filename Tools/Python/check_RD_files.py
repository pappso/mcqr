import os
import re
import subprocess
import sys
import datetime
import shutil

## ajouter modification numeor de version pour local ou cran

report = open(f"docker_output/report_{datetime.datetime.now().date()}.md", "w")
output = subprocess.run(
    ["R", "--version"], capture_output=True, text=True)
print(output.stdout)
report.write("# Compilation and Check Result for Package MCQR\n")
report.write("## Versions of R and dependancies\n")
print("Retrieve version of all packages used in MCQR")
usedpackages = []
output = subprocess.run(
    ["Rscript", "Tools/R/list_dependencies.R"], capture_output=True, text=True)
report.write("|Packages|Version|\n")
report.write("|:--------------------------:|:---------:|\n")
if output.returncode == 0:
    versions = open("packages_version.csv", "r")
    for line in versions:
        if "Packages" not in line:
            if "MCQR" not in line:
                line = line.replace("\"", "")
                #print(line)
                tab = line.strip().split("\t")
                usedpackages.append(tab[0])
                result = "|"+"|".join(tab)+"|\n"
                #print(result)
                report.write(result)
else:
    report.write(output.stdout)
    report.write(output.stderr)
report.write("\n")
report.write("\n")
# os.chdir("../../")
## updating Date package
## retrieving version of package compiled and chck if all packages imported are in use
description = open("MCQR/DESCRIPTION", "r")
in_import = False
report.write("## check of imported dependancies\n")
report.write("\n")
error_import = False
for line in description:
    if "Version: " in line:
        version = line[line.index(":")+1:].strip()
        print(version)
    elif in_import:
        package = line.strip().replace(",", "")
        if package not in usedpackages:
            report.write(
                f"* Warning package {package} still present in DESCRIPTION file but not in use in current code version\n ")
            error_import = True
    elif"Imports:" in line:
        in_import = True
if not error_import:
    report.write(
        "All imported dependancies are currently in use in this version of MCQR\n")
report.write("\n")
description.close()
## updating date argument in DESCRITION file using sed
search = f"s/Date:.*/Date: {datetime.datetime.now().date()}/g"
output = subprocess.run(
    ["sed", "-i", search, "MCQR/DESCRIPTION"], capture_output=True, text=True)

print("compiling MCQR")
report.write("## Compilation of MCQR\n")
report.write(f"Version compiled : {version}\n")
report.write("\n")
filename = "MCQR_"+version+".tar.gz"
output = subprocess.run(
    ["R", "CMD", "build", "--resave-data", "MCQR"], capture_output=True, text=True)
if len(output.stderr) < 10:
    print("Compilation ok")
    report.write(f"successfuly created {filename} package\n")
    report.write(output.stdout)
else:
    print(output.stderr)
    report.write("error during compilation all rest of test are aborted\n")
    report.write("output of R CMD build --resave-data MCQR\n")
    report.write(output.stderr)
    report.close()
    sys.exit()

report.write("\n## Check of MCQR package cran compatibility\n")

## run check of compiled package
print("checking MCQR")
output = subprocess.run(
    ["R", "CMD", "check", "--as-cran", filename], capture_output=True, text=True)


# os.chdir("Tools/Python/")
## checking MCQR.Check to create a todo list

logCheck = open("MCQR.Rcheck/00check.log", "r")

in_note = False
in_warning = False
in_error = False
warning_list = {}
note_list = {}
error_list = {}

temp_catched_content = ""
temp_catched_name = ""
log = ""
for line in logCheck:
    log = log + line
    if line.startswith("*"):
        if in_note:
            note_list[temp_catched_name] = temp_catched_content
            temp_catched_name = ""
            temp_catched_content = ""
            in_note = False
        elif in_warning:
            warning_list[temp_catched_name] = temp_catched_content
            temp_catched_name = ""
            temp_catched_content = ""
            in_warning = False
        elif in_error:
            error_list[temp_catched_name] = temp_catched_content
            temp_catched_name = ""
            temp_catched_content = ""
            in_error = False
        else:
            if "... NOTE" in line:
                in_note = True
                temp_catched_name = re.sub(
                    r"\* checking for |\* checking | ... WARNING| ... NOTE| ... ERROR", "", line).strip()
                # print(line)
                # print(temp_catched_name)
            elif "... WARNING" in line:
                in_warning = True
                temp_catched_name = re.sub(
                    r"\* checking for |\* checking | ... WARNING| ... NOTE| ... ERROR", "", line).strip()
                # print(line)
                # print(temp_catched_name)
            elif "... ERROR" in line:
                in_error = True
                temp_catched_name = re.sub(
                    r"\* checking for |\* checking | ... WARNING| ... NOTE| ... ERROR", "", line).strip()
                # print(line)
                # print(temp_catched_name)
    else:
        if len(line) > 5:
            temp_catched_content += "* "+line+""

report.write("### Error List\n\n")
if len(error_list.keys()) != 0:
    print("Error List :")
    for error in error_list:
        print(error+" :")
        print(error_list[error])
        report.write(f"__{error}__\n\n")
        report.write(error_list[error])
        report.write("\n\n")
else:
    print("No Error detected")
    report.write("No Error detected\n\n")

report.write("### Warning List\n\n")
if len(warning_list.keys()) != 0:
    print("Warning List :")
    for warning in warning_list:
        print(warning+" :")
        print(warning_list[warning])
        report.write(f"__{warning}__\n\n")
        report.write(warning_list[warning])
        report.write("\n\n")
else:
    print("No Warning detected")
    report.write("No Warning detected\n\n")

report.write("### Note List\n\n")
if len(note_list.keys()) != 0:
    print("Note List :")
    for note in note_list:
        print(note+" :")
        print(note_list[note])
        report.write(f"__{note}__\n\n")
        report.write(note_list[note])
        report.write("\n\n")
else:
    print("No Note detected")
    report.write("No Note detected\n\n")

if "missing documentation entries" in warning_list.keys():
    to_parse = warning_list["missing documentation entries"]
    result = ""
    if "Undocumented data sets" in to_parse:
        result = re.findall(r"\‘([a-zA-Z0-9_\.]*)\’", to_parse)
        print(result)
    list_used_data_set = {}
    list_rd = os.listdir("MCQR/man")
    for rd in list_rd:
        temp_file = open("MCQR/man/"+rd, "r")
        for line in temp_file:
            if "data(" in line:
                data_set = re.findall(r"data\((.*)\)", line)
                if len(data_set) != 0:
                    #print(rd)
                    #print(data_set[0])
                    if data_set[0] in result:
                        if data_set[0] in list_used_data_set.keys():
                            list_used_data_set[data_set[0]].append(rd)
                        else:
                            list_used_data_set[data_set[0]] = [rd]
            elif "system.file(\"extdata\"" in line:
                data_set = re.findall(
                    r"extdata\", \"([a-zA-Z0-9_\.]*)\"", line)
                if len(data_set) != 0:
                    #print(rd)
                    #print(data_set[0])
                    if data_set[0] in result:
                        if data_set[0] in list_used_data_set.keys():
                            list_used_data_set[data_set[0]].append(rd)
                        else:
                            list_used_data_set[data_set[0]] = [rd]

    if len(list_used_data_set.keys()) == 0:
        print("Undocumented dataset are not in use in this version")
    else:
        print("Missing Documentation for the following dataset")
        print(list_used_data_set.keys())
report.write("# Content fo log check file\n")

report.write("```log\n")
report.write(log)
report.write("```")
report.close()
shutil.copy2("MCQR.Rcheck/MCQR-manual.pdf", f"docker_output/MCQR-manual.pdf")
shutil.copy2(filename, f"docker_output/{filename}")
shutil.rmtree("MCQR.Rcheck")
