# Version 1.0.2
## Bug fixe
* correction of a bug in mcq.plot.tukey that does not work for cmcq.pepq.ratio.with.ref object
* replacement of aov by lm in mcQ.compute.anova for cmcq.generic.pepq objet and cmcq.generic.pepq.labeling object
* removing of sentence for plto of new graph that does not match for all ide. return to the default message
* replacement of the deprecated par(ask=T) command for multiple graph by devAskNewPage
* correct the error message return to the user in mcq.plot.intensity profile if the factorToColor is not on the metadata
* correction of a bug in mcq.get.metadata that does not work with cmcq.pca object 
* correct the error message in mcq.compute.anova if anova cannot be computed for all measured variable
* correct a bug in mcq.drop and mcq.select function if drop was performed on metadata colnames and if the metadata contains only one user column



# Version 1.0.1
## Bug fixe
* union : correction of a bug where accession were replace by description in the proteins slot
* setdiff : correction of a bug where accession were replace by description in the proteins slot
* intersect : correction of a bug where accession were replace by description in the proteins slot
* mcq.compute.anova.contrast : correction of bug that do not allow computying contrast with only one factor
* mcq.compute.anova.contrast : correction of warning caused by leveneTest



# Version 1.0.0
## new function
* mcq.compute.anova.contrast : function to compute anova using contrast description adapted from Lambert I., Paysant-Le Roux C., Colella S., Martin-Magniette M.-L. (2020) DiCoExpress: a tool to process multifactorial RNAseq experiments from quality controls to co-expression analysis through differential analysis based on contrasts inside GLM models. Plant Methods, 16:68 see mcq.compute.anova.contrast. See help for details
* mcq.select.pvalues.contrast : function to select proteins matching criteria in cmcq.anova.contrast object. See help for details
* mcq.print.contrast.names : function to prin contrast names to facilitate the selection with mcq.select.pvalues.contrast. See help for details
* mcq.select.from.peptlist : function to manipulate peptide list for relevant object. See help for details


## Modification
* mcq.plot.intensity.violin : division of the two graph in case of normalized data to be consistent with mcq.plot.intensity.profile
* mcq.read.diann : adding to option nSeqPerProt allow to select the minimal number of different sequence required to consider a proteine and inference only one option for the moment specific that select only Protein.Group with one accession. 
* mcq.compute.anova : adding a trycatch duirng computation of model with clean error message for protein without variation
* mcq.compute.anova : modification of argument name inter to interaction to be consistent with newly mcq.compute.anova.contrast function
* mcq.get : correction of a bug with cmcq.ratio object
* mcq.plot.tukey : suppression of argument qprot. Data were now take form cmcq.anova object and stocked in cmcq.tukey object
* mcq.plot.violin : correction of a bug that produce empty plot
* mcq.compute.anova : now can work with peptide level and labelling object at peptide level
* mcq.select.pvalues : retturn a cmcq.protlisst or cmcq.peplist dependanding one which type of objecct was used in mcq.ccompute.anova


## Removed function
* mcq.get.compar : replace by mcq.get. Use wide format to have an compar table. See mcq.get function  help for details
* mcq.get.dataframe : replace by mcq.get. Use long format to have an compar table. See mcq.get function  help for details
* mcq.write.compare : replace by mcq.write design to work with output of mcq.get function . See mcq.write function  help for details
* mcq.write.dataframe : replace by mcq.write design to work with output of mcq.get function . See mcq.write function  help for details
* mcq.write.merge : replace by mcq.write design to work with output of mcq.get function . See mcq.write function  help for details
* mcq.write.synthesis : removed



# Version 0.6.15
## Modification 
* adding proForma column for data coming from diann output
* removing dplyr dependancies
* removing zero values in diann files that cause some bug on normalization

## bug fix
* correction of a bug in mcg.get for data coming from diann output files
* correction of a bug in mcq.get and mcq.get.dataframe for data with labeling caused by proForma column
* correction of a bug in mcq.get.merge due to the usage of mcq.get function instead of mcq.get.compar



# Version 0.6.14
## bug fix
* Correction bug on mcq.compute.t.test for object of class cmcq.pepq.ratio.with.ref 
* Correction bug on mcq.get.dataframe for object of class cmcq.pepq.ratio.with.ref 
* Correction bug on mcq.get for object cmcq.pepq.ratio.with.ref and 
* Correction bug on mcq.compute.anova on mean intensity calculation that perform on logarea instead of area
## Bug Known 
* Execution not possible for mcq.get.dataframe and mcq.get for object of class cmcq.pepq.masschroq.labelling


# Version 0.6.13 (experimental)
## new function
* mcq.get : function design to replace mcq.get.dataframe and mcq.get.compar, the function can produce wide or long format depending on object with format argument. The function reimplement all other functionality of mcq.get.dataframe and mcq.get.compar excepted the ability to direct compute factor by mean that is redundant with mcq.compute.quantity.by.factor

## Modification
* mcq.get.msstats function can also take into account labeled peptides
* take into account the new proForma column on proteins slot (added by masschroq software)
* correction of mcq.get.dataframe for cmcq.pepq.ratio.with.ref
* modification of mcq.compute.anova for lm to ensure type III anova using car::Anova function instead of base::anova

# Version 0.6.11
## Modification
* adding no.norm method to normalization to be able to process all the script without normalization
* renaming of "median" and "median.RT" method to "diff.median" and "diff.median.RT" on mcq.compute.normalization
* adding new median and median.RT method for normalization based on how its performed in msstat package (M. Choi et al. “MSstats: an R package for statistical analysis for quantitative mass spectrometry-based proteomic experiments.” Bioinformatics (2014), 30 (17): 2524-2526)
* adding warning message if mcq.compute.peptide.imputation is performed after applied no.norm method in mcq.compute.normalization

# Version 0.6.10
## new function
* mcq.get.msstats : function that extract dataframe from mcqr object (only cmcq.pepq.masschroq) to create a dataframe compatible with msstats package. See help for more details

# Version 0.6.9
## modification
* data file are moved from extdata directory to dataverse site to redeuce the size of the package. the exemple section of RD are modify to present the new way of accession of raw data
* modification on mcq.plot.intensity.profile function to be able to work with object with data imputation
* modification on mcq.plot.intensity.profile function to allow "msrun" value on factorToColor argument
* modification on mcq.plot.intensity.violin function to allow "msrun" value on factorToColor argument

## bug fix
* correction of a bug on mcq.compute.pca that not allow computijng pca on cmcq.pepq.by.track object
* adding is_fraction varialbe to cmcq.pepq.by.track object for compatibility reason
* replacement of old named mcq.compute.protq.by.track and mcq.compute.pepq.by.track by mcq.compute.quantity.by.track function name on lanceur_reference_sds.R scenario file
* correction of an uncatched error if you submit a protlist with an empty dataframe to mcq.compute.tukey
* correction of name argument paramlist in cmcq.cv for compatibility



# Version 0.6.8
## Bug Fix
* correction of a bug in mcq.compute.protein.abundance for topN method that make sum of Log value than area values (Thanks to olivier Langella)


# Version 0.6.7
## modification
* add TopN method for protein abundance computation

# Version 0.6.6
## modification
* modification of the way that file a read (data file and metadata file) to allow loading from url with check of availability of the ressource files
* adding possibility to filter on peak quality to mcq.drop function (! warning filter removing all peak with low selected quality without verification of qualitiy for other peak of the same peptiz )


## Bug Fix
* correction of a bug on mcq.get.Dataframe for cmcq.ratio object
* correction of a bug in mcq.plot.protein.abundace introduce in 0.6.5




# Version 0.6.5
First submission in cran

* bug fixe in mcq.write.scenarios that does not write lanceur_reference_phospo.R
* update description in DESCRIPTION and mcqr-package.Rd and details in mcqr-package.Rd
* update authors list and role for cran submission
* modification of call 'if(class(object) == "name of class")' by 'if(is(object, "name of class"))' for cran compatibility


# Version 0.6.4
New function "mcq.write.scenarios(directory= ".")" that write in selected directory scenarios for analysis (see help for more details)

* Correction of a bug in mcq.get.dataframe for labelling data that result in a error
* Correction of a bug in mcq.drop.unreproducible.peptide that does not allowed to be done in refined mode (thanks to julien Jardin for bug report)
* Correction of a bug in mcq.compute.anova in case of peakcounting data that return nothing (thanks to julien Jardin for bug report)
* correction of a warning on mcq.get.dataframe due to bad usage of order function (thanks to julien Jardin for bug report)


# Version 0.6.3

* Correction of a bug in priv.mcq.merge.metadata.labeling that does not test correctly colnames and result in an error
* Correction of a bug in mcq.plot.rt.alignement that raise an error if the number of file is not a multiple of number of graph on pages


# Version 0.6.2

* Major modification on mcq.compute.cluster and mcq.plot.cluster, the function is now able to compute clusters with different method and clusters numbers and output a comparison off quality of each method/numbers. See help for more details. the function plot was modify to be able to plot a cluster in the list of tested ones.
* Modification of function mcq.get.compar to be able to work with new cmcq.clusters structure.
* Creation of mcq.select.cluster to be able to select cluster of interest in a cmcq.cluster object. See help for details
* Mcq.write.metadata.template : adding type option to specify the type of file exported. See help for more details
* Correction of a bug in mcq.compute.protein.imputation that do not impute proteins
* Correction of a bug in mcq.compute.anova that select glm model for SpectralCounting object with normalised data instead of lm model with square root transformation
* Correction of a bug in mcq.compute.anova that raise an error and does not permit to perform anova with glm Model
* Correction of a bug in mcq.plot.peptide.intensity



# Version 0.6.1

* Rename mcq.merge.metadata to mcq.add.metadata
* New function mcq.write that take the output of mcq.get.dataframe and cq.get.compar and write in ods file or tsv/csv files example
** `mcq.write(object, type="flat", sep="\t", dec=".", file="export")` for tsv file
** `mcq.write(object, type="ods", file="export")` for ods files
* Add goodModel option to mcq.select.values to specify if proteins with a bad model estimation for glm computation should be exported or not, Example :
** `mcq.select.pvalues(object, padjust=TRUE, alpha=0.05, flist=NULL, goodModel=TRUE)` to retrieve only proteins with good model for selected pvalues
** `mcq.select.pvalues(object, padjust=TRUE, alpha=0.05, flist=NULL, goodModel=TRUE)` to retrieve all proteins for selected pvalues
* Modification of mcq.compute.anova to redirect warnings messages to an output file 'Warnings_anova_GLM.txt'. Proteins with warnings or error are not added to the result object.
* Modification of mcq.compute.tukey to add information about the evolution of the computation
* Add test on mcq.compute.cluster to block the execution if there is only to levels in the factor or combination of factor used for computation with a relevant warning message
* Correct a bug on the test of metadata columns names in the file that permit to have forbidden columns. The error message is now clear.
PeakCounting support is discontinued. Error may be always present concernings this part of the script in this version
* correction of a error in mcq.compute.pca with spectralcoutning data that not allowed to plot pca if there is only one factor choosed for labels with pca object made with the sames factor
* Remove a limitation in mcq.drop.uncorrelated.peptide to permit computation of correlation even is there is less than 9 peptide for a protein.
* update help for mcq.write.compar in case of writing file from pca object
