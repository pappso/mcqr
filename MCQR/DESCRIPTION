Package: MCQR
Type: Package
Title: a R Package for Analysis of Mass-Spectrometry-Based Proteomics Data
Version: 1.0.3
Date: 2025-03-07
Authors@R: c(person("Balliau", "Thierry", email="thierry.balliau@inrae.fr", role=c("aut", "cre")),
			person("Frambourg", "Anne", email="anne.frambourg@inrae", role=c("aut")),
			person("Langella", "Olivier", email="olivier.langella@universite-paris-sud.fr", role=c("aut")),
			person("Martin", "Marie-Laure", email="mlmartin@agroparistech.fr", role=c("aut")),
			person("Zivy", "Michel", email="michel.zivy@universite-paris-saclay.fr", role=c("aut")),
			person("Blein-Nicolas", "Mélisande", email="melisande.blein-nicolas@inrae.fr", role=c("aut"))
			)
Description: Standardised toolkit to process and analyse quantification data from bottom-up proteomics experiments, including label-free shotgun experiments, isotopic labeling experiments, fractionation-based experiments and experiments based on enrichment in post-translational modifications. MCQR contains all the necessary functions to analyse spectral counts (SC) or extracted ion current (XIC) data. It also contains functions to export analysis results as graphical representations in pdf format or as data tables in tsv or txt format. MCQR is directly interoperable with MassChroQ (Valot et al. 2011, <doi:10.1002/pmic.201100120>) and X!TandemPipeline (Langella et al. 2017, <doi:10.1021/acs.jproteome.6b00632>). It can also take input data from other quantification software provided that they are adequately formatted.
URL: https://forgemia.inra.fr/pappso/mcqr
Encoding: UTF-8
License: GPL (>= 3)
Depends: R (>= 4.0.0), methods
Imports:
	stringr,
	readODS (>= 1.6.4),
	VIM,
	ade4,
	gplots,
	clValid,
	agricolae,
	ggplot2,
	RColorBrewer,
	plotly,
	plyr,
	data.table,
	nlme,
	ggVennDiagram,
	car
Suggests:
	dataverse
