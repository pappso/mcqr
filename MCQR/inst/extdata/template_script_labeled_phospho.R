# template script for phosphoproteomics analysis with labeling and fractionation v.1.0.3

# This R script is designed to process and analyze data obtained from an experiment including peptide labeling, sample multiplexing, phosphopeptide enrichment, and multiplex fractionation.
# It can be used with the demo datasets available for download at https://entrepot.recherche.data.gouv.fr/dataset.xhtml?persistentId=doi:10.57745/EW6BSO.
# Ensure that R and MCQR are already installed on your computer before proceeding (see https://forgemia.inra.fr/pappso/mcqr for instructions on how to install MCQR).


#-------------------------------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------- TABLE OF CONTENT ---------------------------------------------------------------------------------------------

# 0. Downloading the demo data

# 1. Setting up the R session

# 2. Analyzing quantification data obtained from extracted ion chromatograms (XIC)
# 	2.1. Loading data and metadata 
# 	2.2. Quality check
# 	2.3. Data filtering
# 	2.4. Intensity normalization
# 	2.5. Post-normalization filtering
# 	2.6. Descriptive analyses
#	2.7. Hypothesis testing
# 	2.8. Exporting results

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------


#**************************************************************************************************************************************************
#**************************************************************************************************************************************************
#******************************************************   0. Downloading the demo data
#**************************************************************************************************************************************************
#**************************************************************************************************************************************************

# You can dowload the data manually from https://entrepot.recherche.data.gouv.fr/dataset.xhtml?persistentId=doi:10.57745/EW6BSO or by using the following R commands (requires the library 'dataverse').
	library("dataverse")
	
# Set the path to the working directory where the data will be downloaded (replace '/path/to/my_working_directory' with the actual path on your computer).
	path <- "/path/to/my_working_directory"
	setwd(path)
	
# Create a directory where to import the data (or you can do this using in your file manager).
	newfolder <- "input"
	dir.create(file.path(path, newfolder))

# Download the datasets.
	# Phosphoproteomics data (the files are compressed in a zip)
		## download the zip file
	file.name1 <-"compressed_quanti_data_phospho.zip"
	file1 <- get_file_by_name(filename=file.name1, dataset = "10.57745/EW6BSO", server = "entrepot.recherche.data.gouv.fr")
	file.create(file.name1)
	writeBin(file1, paste(path,"input", file.name1, sep="/"))
	
		## unzip the file
	unzip(paste(path, "input", file.name1, sep="/"), exdir=paste(path, "input", sep="/"))
   
 	# Metadata
	file.name2 <-"filled_metadata_phospho.ods"
	file2 <- get_file_by_name(filename=file.name2, dataset = "10.57745/EW6BSO", server = "entrepot.recherche.data.gouv.fr")
	file.create(file.name2)
	writeBin(file2, paste(path,"input", file.name2, sep="/"))   
 
# Clean the workspace.
	rm(list=ls())
    


#**************************************************************************************************************************************************
#**************************************************************************************************************************************************
#******************************************************   1. Setting up the R session
#**************************************************************************************************************************************************
#**************************************************************************************************************************************************

# Load the MCQR package.
	library("MCQR")

# Check the package version.
	packageVersion("MCQR")
	
# Check the number of threads being used by MCQR and adjust them if needed.
	mcq.get.max.threads()
	mcq.set.max.threads(threads = 2)	

# Check the default color palette and change it if desired.
	mcq.get.color.palette()
	mcq.set.color.palette("Dark2")

# Set the path to the working directory that contains the data (replace '/path/to/my_working_directory' with the actual path on your computer).
	path <- "/path/to/my_working_directory" #"/home/blein/Meli/Documents/mcqr/new_example_data/template_phospho"
	setwd (path)	
	
# Create a directory to export the results (or you can do this using your file manager).
	newfolder <- "output"
	dir.create(file.path(path, newfolder))

# Set the seed using the set.seed function to ensure reproducible results when functions involve random variables.
	set.seed(30)


#**************************************************************************************************************************************************
#**************************************************************************************************************************************************
#******************************************************   2. Analyzing quantification data obtained from extracted ion chromatogram (XIC)
#**************************************************************************************************************************************************
#**************************************************************************************************************************************************

#********************************************************
#--------------------------------------------------------
#             2.1 Loading data and metadata
#--------------------------------------------------------
#********************************************************

# Load the data. Note: Peptide intensities are automatically log10-transformed.
	raw_xic <- mcq.read.masschroq.fraction(directory = "input/result_20160412_Meimoun_143_i2masschroq1_0_16.d", labeling = TRUE)
	
# Overview of the content of the 'raw_xic' object.
	summary (raw_xic)	

# Export a template file for the metadata. The file will be saved in '/path/to/my_working_directory/output' in ODS format.
	mcq.write.metadata.template(raw_xic, file="output/metadata_template", type="ods")
	
#!!!!!!!!!!!!!!!!!!!!! Open the template file in Libreoffice or Excel, fill in the columns with all the information about your MS runs, and save it as a new ODS file. Here, the filled metadata is already available: '/path/to/my_working_directory/input/filled_metadata.ods'. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# Import the metadata.
	my_metadata <- mcq.read.metadata("input/filled_metadata_phospho.ods")
	
# Check the content of the metadata.
	summary(my_metadata)

# Associate the metadata with the quantification data.
	raw_xic <- mcq.add.metadata(raw_xic, metadata=my_metadata)


#********************************************************
#--------------------------------------------------------
#             2.2. Quality check (QC)
#--------------------------------------------------------
#********************************************************

#############  QC based on experimental retention time (RT) measurements

# Distribution plot of the chromatographic peak widths
	# for the entire dataset
	mcq.plot.peak.width(raw_xic, limit=200, file="output/peak_width.pdf")
	# for each fraction and each label
	mcq.plot.peak.width(raw_xic, limit=200, byFraction = TRUE, byLabel = TRUE, file="output/peak_width_by_fraction_by_label.pdf")

# Distribution plot of the variability in RT measurements
	# for each fraction
	mcq.plot.rt.variability(raw_xic, limit = 80, file="output/RT_variability.pdf")
	# for each fraction and each label
	mcq.plot.rt.variability(raw_xic, limit = 80, byLabel = TRUE, file="output/RT_variability_by_label.pdf")

# Distribution of RT differences between two labels. The 'ref' argument allows you to speicfy which label should be used as the reference
	# for the entire dataset	
	mcq.plot.delta.rt(raw_xic, ref="light", file="output/RT_difference_between_labels.pdf")
	# for each fraction
	mcq.plot.delta.rt(raw_xic, ref="light", byFraction = TRUE, file="output/RT_difference_between_labels_by_fraction.pdf")

	
#############  Intensity-based QC

## Intensity distribution plot
	# for each MS run
	mcq.plot.intensity.distribution(raw_xic, byTrack=FALSE, overlay=TRUE, file="output/intensity_distribution_by_msrun.pdf")
	# for each multiplex (or track)	
	mcq.plot.intensity.distribution(raw_xic, byTrack=TRUE, overlay=TRUE, file="output/intensity_distribution_by_multiplex.pdf")

#############  QC based on chromatographic peaks
	mcq.plot.counts(raw_xic, overlay=TRUE, file="output/count_peptide-mz.pdf")
	
#############  QC on fractions
# Distribution of the number of fractions in which a peptide-mz is present
	mcq.plot.fractions.per.peptide(raw_xic, file="output/peptide-mz_fraction.pdf")

#############  QC on phosphorylated peptides-mz
# Number of phosphorylated peptides-mz in each MS run
	# as a cumulated barplot
	mcq.plot.peptide.modification(raw_xic, modifiedAA = c("S", "T", "Y"), massDelta = 79.97, type="counts", position= "stack", file="output/peptide_mod_stack.pdf")
	# as juxtaposed barplots
	mcq.plot.peptide.modification(raw_xic, modifiedAA = c("S", "T", "Y"), massDelta = 79.97, type="counts", position= "dodge", file="output/peptide_mod_dodge.pdf")


#********************************************************
#--------------------------------------------------------
#       2.3. Data filtering
#--------------------------------------------------------
#********************************************************

############# Create a new object to store the filtered data.
	filtered_xic <- raw_xic
	
############# Remove peptides-mz with excessive peak widths.	
# Use the plot produced by mcq.plot.peak.width to determine the appropriate on the cut-off. Avoid being too stringent with this filter, as peak width is proportional to the peak height. A large peak may simply indicate a peptide-mz that is particularly intense.
	filtered_xic = mcq.drop.wide.peaks(filtered_xic, cutoff = 200, byFraction = FALSE, verbose = TRUE)

############# Remove peptides-mz with highly variable RTs.
# Use the plot produced by mcq.plot.rt.variability to determine the appropriate cut-off. You can decide whether a peptide-mz should be removed from the entire experiment or only from the fraction(s) where the RT is highly variable using the "byFraction" argument.
	filtered_xic <- mcq.drop.variable.rt(filtered_xic, cutoff = 20, byFraction = TRUE, verbose=TRUE)

############# Remove peptide-mz with a large RT difference between labels.
# Use the plot produced by mcq.plot.delta.rt to determine the appropriate the cut-off. You can decide whether a peptide-mz should be removed from the entire experiment or only from the fraction(s) where the RT difference is high using the "byFraction" argument.
	filtered_xic <- mcq.drop.delta.rt(filtered_xic, cutoff = 15, byFraction = TRUE, ref = "light", verbose = TRUE)


#********************************************************
#--------------------------------------------------------
#       2.4. Intensity normalization
#--------------------------------------------------------
#********************************************************	

############# Intra-multiplex normalization
# Normalize the intensities within each multiplex (or track). For this, compute the intensity ratio using a label as the reference. If a peptide-mz is observed with different charge states and/or across multiple fractions, the ratio is computed for the most abundant charge state in the fraction where the peptide-mz is the most intense.
	xic_ratio = mcq.compute.label.ratio(filtered_xic, ref="light")

# Violin plots of intensity ratios
	# for each label (except the label chosen as the reference)
	mcq.plot.ratio.violin(xic_ratio, byLabel=TRUE, file="output/violin_intensity_ratio_by_label.pdf")
	# for each multiplex
	mcq.plot.ratio.violin(xic_ratio, byLabel=FALSE, file="output/violin_intensity_ratio_by_multiplex.pdf")

# Distribution plots of intensity ratio
	# for each label (except the label chosen as reference)
	mcq.plot.ratio.distribution(xic_ratio, byLabel=TRUE, overlay=TRUE, file="output/distrib_intensity_ratio_by_label.pdf")
	# for each multiplex (or track)
	mcq.plot.ratio.distribution(xic_ratio, byLabel=FALSE, overlay=TRUE, file="output/distrib_intensity_ratio_by_multiplex.pdf")

############# Normalize the intensities across the different multiplex x label combinations.
# The method implemented in MCQR to normalize intensity ratios adjusts the median of the ratios to 0 for each combination of multiplex x label.
	normalized_xic = mcq.compute.normalization(xic_ratio)

############# Check the normalization by comparing before-and-after representations.
	mcq.plot.ratio.violin(normalized_xic, byLabel=TRUE, file="output/violin_intensity_ratio_by_label_after_norm.pdf" )
	mcq.plot.ratio.distribution(normalized_xic, byLabel=TRUE, overlay=TRUE, file="output/distrib_intensity_ratio_by_label_after_norm.pdf")

	
#********************************************************
#--------------------------------------------------------
#       2.5. Post-normalization filtering
#--------------------------------------------------------
#********************************************************

############# Create a new object to store filtered and normalized data.	
	normalized_filtered_xic <- normalized_xic

############# Filter for non-reproducible peptides-mz.	
	# Plot of peptide-mz reproducibility
	mcq.plot.peptide.reproducibility(normalized_filtered_xic, file="output/peptide_reproducibility.pdf")
	# Remove unreproducible peptides-mz.	
	normalized_filtered_xic <- mcq.drop.unreproducible.peptides(normalized_filtered_xic, method = "refined", percentNA = 0.35, flist=c("time", "light_regime"))

############# Select for phosphorylated peptides-mz.
	normalized_filtered_xic <- mcq.select.modified.peptides(normalized_filtered_xic, modifiedAA = c("S", "T", "Y"), massDelta = 79.97)

############# Display of a summary of the filtered data.
	summary(normalized_filtered_xic)
	
	
#********************************************************
#--------------------------------------------------------
#       2.6. Descriptive analyses
#--------------------------------------------------------
#********************************************************	

#############  PCA representation
	pca_on_phosphopep <- mcq.compute.pca(normalized_filtered_xic, flist=c("time", "light_regime"))
	mcq.plot.pca(pca_on_phosphopep, factorToColor=c("light_regime"), labels =c("time"), tagType="label", labelSize=3, file="output/pca_phospho.pdf")

############# Heatmap representation
	mcq.plot.heatmap(normalized_filtered_xic, flist=c("light_regime", "time"), factorToColor="light_regime", protLab=FALSE, file="output/heatplot.pdf")
	
	
#********************************************************
#--------------------------------------------------------
#       2.7. Hypothesis testing
#--------------------------------------------------------
#********************************************************	

############# Perform an analysis of variance (ANOVA). Analysis of variance (ANOVA)
	anova_on_phosphopep <- mcq.compute.anova(normalized_filtered_xic, flist=c("light_regime", "time"))
	
############# Select proteins with non zero parameters for each factor in the ANOVA model.
	phospho_non_zero_time <- mcq.select.pvalues(anova_on_phosphopep, padjust=TRUE, 0.05, flist="time")
	quanti_phospho_non_zero_time <- mcq.select.from.peplist(normalized_filtered_xic, phospho_non_zero_time)
	
	phospho_non_zero_light <- mcq.select.pvalues(anova_on_phosphopep, padjust=TRUE, 0.05, flist="light_regime") 
	quanti_phospho_non_zero_light <- mcq.select.from.peplist(normalized_filtered_xic, phospho_non_zero_light)
	
	phospho_non_zero_lighttime <- mcq.select.pvalues(anova_on_phosphopep, padjust=TRUE, 0.05, flist=c("light_regime", "time"))
	quanti_phospho_non_zero_lighttime <- mcq.select.from.peplist(normalized_filtered_xic, phospho_non_zero_lighttime)
	
############# Perform post-hoc Tukey tests (not working yet).
	tukey_time <- mcq.compute.tukey(anova_on_phosphopep, flist="time", selected_list=phospho_non_zero_time)
	
#############  Graphical representations of the results from hypothesis testing 
	# Distribution of ANOVA p-values
	mcq.plot.pvalues(anova_on_phosphopep, file="output/pvalues.pdf")

	# Volcano plot
	mcq.plot.volcano(anova_on_phosphopep, file="output/volcano.pdf")	

	# Display the results of the Tukey tests on boxplots of protein abundances. (not working yet).
	mcq.plot.tukey(tukey_time, factorToColor="time", file="output/tukey_time.pdf")

	
#********************************************************
#--------------------------------------------------------
#       2.9. Exporting results
#--------------------------------------------------------
#********************************************************

############# Extract and export PCA results.
	pca_on_phospho_results <- mcq.get(pca_on_phosphopep)
	mcq.write(pca_on_phospho_results, file=("output/pca_on_phospho_results"))

############# Extract and export protein quantification data
	phosphopep_abundance_table <- mcq.get(normalized_filtered_xic)
	mcq.write(phosphopep_abundance_table, file="output/phosphopep_abundance_table", type="ods")

############# Extract and export ANOVA results.  
	my_anova_results <- mcq.get(anova_on_phosphopep)
   	mcq.write(my_anova_results, file="output/anova_results", type="ods")
   	
############# Extract and export a table merging ANOVA results with protein quantification data (not working yet).
	merge_anova_quanti <- mcq.get.merge(object=normalized_filtered_xic, object2=anova_on_phosphopep)
	mcq.write(merge_anova_quanti, file="output/merged_results", type="ods")

