# template script fractionation v.1.0.3

# This R script is designed to process and analyze data obtained by label-free proteomics with sample fractionation. 
# It can be used with the demo data sets available for download at https://entrepot.recherche.data.gouv.fr/dataset.xhtml?persistentId=doi:10.57745/E1Z1QX
# Ensure that R and MCQR are already installed on your computer before proceeding (see https://forgemia.inra.fr/pappso/mcqr for instructions on how to install MCQR).

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------- TABLE OF CONTENT ---------------------------------------------------------------------------------------------

# 0. Downloading the demo data

# 1. Setting up the R session

# 2. Analyzing quantification data obtained from extracted ion chromatograms (XIC)
# 	2.1. Loading data and metadata
# 	2.2. Quality check
# 	2.3. Data filtering
#       2.4. Summarizing fractions to the sample level
# 	2.5. Intensity normalization
# 	2.6. Post-normalization filtering
# 	2.7. Imputing missing values and aggregating peptide intensities at the protein level 
# 	2.8. Descriptive analyses
#	2.9. Hypothesis testing with ANOVA and Tukey test
# 	2.10. Exporting results

# 3. Analyzing spectral counting (SC) data
# 	3.1. Loading data and metadata
# 	3.2. Quality check
# 	3.3. Summarizing fractions to the sample level
# 	3.4. Protein filtering
# 	3.5. Descriptive analysis
#	3.6. Hypothesis testing
# 	3.7. Exporting results 



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------

#**************************************************************************************************************************************************
#**************************************************************************************************************************************************
#******************************************************   0. Downloading the demo data
#**************************************************************************************************************************************************
#**************************************************************************************************************************************************

# You can dowload the data manually from https://entrepot.recherche.data.gouv.fr/dataset.xhtml?persistentId=doi:10.57745/E1Z1QX or by using the following R commands (requires the library 'dataverse').
	library("dataverse")
	
# Set the path to the working directory where the data will be downloaded (replace '/path/to/my_working_directory' with the actual path on your computer).
	path <- "/path/to/my_working_directory"
	setwd(path)
	
# Create a directory where to import the data (or you can do this using in your file manager).
	newfolder <- "input"
	dir.create(file.path(path, newfolder))
	
# Download the datasets.
	# Fractionation data
			## download the zip file
	file.name1 <-"fractionation_data.zip"
	file1 <- get_file_by_name(filename=file.name1, dataset = "10.57745/E1Z1QX", server = "entrepot.recherche.data.gouv.fr")
	file.create(file.name1)
	writeBin(file1, paste(path, "input", file.name1, sep="/"))
	
		## unzip the file
 	unzip(paste(path, "input", file.name1, sep="/"), exdir=path)
 	
 	# Metadata
	file.name2 <-"filled_metadata.ods"
	file2 <- get_file_by_name(filename=file.name2, dataset = "10.57745/E1Z1QX", server = "entrepot.recherche.data.gouv.fr")
	file.create(file.name2)
	writeBin(file2, paste(path, "input", file.name2, sep="/")) 
 
# Clean the workspace.
	rm(list=ls())
    


#**************************************************************************************************************************************************
#**************************************************************************************************************************************************
#******************************************************   1. Setting up the R session
#**************************************************************************************************************************************************
#**************************************************************************************************************************************************

# Load the MCQR package.
	library("MCQR")

# Check the package version.
	packageVersion("MCQR")
	
# Check the number of threads being used by MCQR and adjust them if needed.
	mcq.get.max.threads()
	mcq.set.max.threads(threads = 2)	

# Check the default color palette and change it if desired.
	mcq.get.color.palette()
	mcq.set.color.palette("Dark2")

# Set the path to the working directory that contains the data (replace '/path/to/my_working_directory' with the actual path on your computer).
	path <- "/path/to/my_working_directory" 
	setwd (path)
	
# Create a directory to export the results (or you can do this using your file manager).
	newfolder <- "output"
	dir.create(file.path(path, newfolder))

# Set the seed using the set.seed function to ensure reproducible results when functions involve random variables.
	set.seed(30)




#**************************************************************************************************************************************************
#**************************************************************************************************************************************************
#******************************************************   2. Analyzing quantification data obtained from extracted ion chromatograms (XIC)
#**************************************************************************************************************************************************
#**************************************************************************************************************************************************


#********************************************************
#--------------------------------------------------------
#             2.1 Loading data and metadata
#--------------------------------------------------------
#********************************************************

# Load the data. Note: Peptide intensities are automatically log10-transformed.
	raw_xic <- mcq.read.masschroq.fraction(directory="input")

# Overview of the content of the 'raw_xic' object
	summary (raw_xic)

# Export a template file for the metadata. The file will be saved in '/path/to/my_working_directory/output' in ODS format.
	mcq.write.metadata.template(raw_xic, file="output/metadata_template", type="ods")

#!!!!!!!!!!!!!!!!!!!!! Open the template file in Libreoffice or Excel, fill in the columns with all the information about your MS runs, and save it as a new ODS file. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# Import the metadata.
	my_metadata <- mcq.read.metadata("input/filled_metadata.ods")
	
# Check the content of the metadata.
	summary(my_metadata)

# Associate the metadata with the quantification data.
	raw_xic <- mcq.add.metadata(raw_xic, metadata=my_metadata)


#********************************************************
#--------------------------------------------------------
#             2.2. Quality check (QC)
#--------------------------------------------------------
#********************************************************

#############  QC based on experimental retention time (RT) measurements
# Distribution plot of the chromatographic peak widths
	mcq.plot.peak.width(raw_xic, byFraction = FALSE, file="output/peak_width_raw.pdf")
	mcq.plot.peak.width(raw_xic, byFraction = TRUE, file="output/peak_width_raw_by_fraction.pdf")

# Distribution plot of the variability in RT measurements
	mcq.plot.rt.variability(raw_xic, width=50, file="output/rt_var_raw.pdf")
	mcq.plot.rt.variability(raw_xic, limit=50, file="output/rt_var_zoom_raw.pdf") 

#  Profile of the median intensity along the chromatography
	mcq.plot.intensity.profile(raw_xic, factorToColor=c("fraction"), file="output/profile_raw.pdf") 

#############  Intensity-based QC
#  Intensity distribution plot
	mcq.plot.intensity.distribution(raw_xic, width=0.2, file="output/intensity_distrib_raw.pdf") 

#############  QC based on chromatographic peaks
	mcq.plot.counts(raw_xic, file="output/counts_per_injection.pdf")

############# Fraction-based QC
# Number of fractions per peptide-mz
	mcq.plot.fractions.per.peptide(raw_xic, file="output/fractions_per_peptide_mz.pdf")

# SDS-page-like representation
	mcq.plot.sds.page(raw_xic, file="output/sds_page.pdf")


#********************************************************
#--------------------------------------------------------
#       2.3. Data filtering
#--------------------------------------------------------
#********************************************************

############# Create a new object to store the filtered data.
	filtered_xic <- raw_xic

#############  If necessary, remove fractions
	filtered_xic_without_B1 <- mcq.drop(raw_xic, factor="fraction", levels=c("B1"))

# The difference between the list of proteins in raw_xic and the list of proteins in filtered_xic_without_B1 gives the list of protein that are only in the removed fractions.
	protlist1 <- mcq.get.protlist(raw_xic)
	protlist2 <- mcq.get.protlist(filtered_xic_without_B1)
	prot_B1 <- setdiff(protlist1, protlist2)

############# Remove peptides-mz with excessive peak widths.
# Use the plot produced by mcq.plot.peak.width to determine the appropriate on the cut-off. Avoid being too stringent with this filter, as peak width is proportional to the peak height. A large peak may simply indicate a peptide-mz that is particularly intense.
	filtered_xic <- mcq.drop.wide.peaks(filtered_xic, cutoff=150)

############# Remove peptides-mz with highly variable RTs.  
# Use the plot produced by mcq.plot.rt.variability to determine the appropriate cut-off.
	filtered_xic = mcq.drop.variable.rt(filtered_xic, cutoff=20, byFraction = TRUE)
	
############# Display of a summary of the filtered data.
	summary(filtered_xic)


#********************************************************
#--------------------------------------------------------
#       2.4. Summarizing fractions to the sample level
#--------------------------------------------------------
#********************************************************	

############# Compute peptide intensities by track as the sum of the peptide intensities of all fractions for each track
	filtered_xic_by_track <- mcq.compute.quantity.by.track(filtered_xic)
	summary(filtered_xic_by_track)

#############  QC for the reconstituted samples (tracks).
#  Violin plot
	mcq.plot.intensity.violin(filtered_xic_by_track, factorToColor=c("genotype", "environment"), file="output/violin.pdf")

# Intensity correlation plots (here you have to provide a reference track)
	mcq.plot.intensity.correlation(filtered_xic_by_track, ref="G1-E1-3", file="output/correlation.pdf") 


#********************************************************
#--------------------------------------------------------
#       2.5. Intensity normalization
#--------------------------------------------------------
#********************************************************

############# Compare different normalization methods.	
  	method1=mcq.compute.normalization(filtered_xic_by_track, method="median")
	pca_on_method1 = mcq.compute.pca(method1) 
	mcq.plot.intensity.violin(method1, factorToColor=c("genotype", "environment"), file="output/violin_method1.pdf")	
	mcq.plot.pca(pca_on_method1, factorToColor=c("genotype", "environment"), tagType="label", labels=c("genotype", "environment"), file="output/pca_filtered_method1.pdf" ) 

     	method2=mcq.compute.normalization(filtered_xic_by_track, method="diff.median", ref="G1-E1-3")
	pca_on_method2 = mcq.compute.pca(method2) 
	mcq.plot.intensity.violin(method2, factorToColor=c("genotype", "environment"), file="output/violin_method2.pdf")	
	mcq.plot.pca(pca_on_method2, factorToColor=c("genotype", "environment"), tagType="label", labels=c("genotype", "environment"), file="output/pca_filtered_method2.pdf" ) 
	
   	method3=mcq.compute.normalization(filtered_xic_by_track, method="percent")
	pca_on_method3 = mcq.compute.pca(method3) 	
	mcq.plot.intensity.violin(method3, factorToColor=c("genotype", "environment"), file="output/violin_method3.pdf")
	mcq.plot.pca(pca_on_method3, factorToColor=c("genotype", "environment"), tagType="label", labels=c("genotype", "environment"), file="output/pca_filtered_method3.pdf" ) 
		
############# Normalize the intnesities using the chosen method.
	normalized_xic=mcq.compute.normalization(filtered_xic_by_track, method="diff.median")


#********************************************************
#--------------------------------------------------------
#       2.6. Post-normalization filtering
#--------------------------------------------------------
#********************************************************

############# Create a new object to store the filtered and normalized data.	
	normalized_filtered_xic=normalized_xic

############# Filter for shared peptides.
	# Plot of shared peptides
	mcq.plot.peptide.sharing(normalized_filtered_xic, file="output/shared.pdf")
	# Remove the shared peptides.
	normalized_filtered_xic = mcq.drop.shared.peptides(normalized_filtered_xic)

############# Filter for non-reproducible peptides-mz.	
	# Plot of peptide-mz reproducibility
	mcq.plot.peptide.reproducibility(normalized_filtered_xic, file="output/repro.pdf")
	# Remove unreproducible peptides-mz.	
	normalized_filtered_xic = mcq.drop.unreproducible.peptides(normalized_filtered_xic, method="percent", percentNA=0.1)

########### Filter for uncorrelated peptides-mz.
	# Compare 2 parameters setting for coefficient of correlation
	protein_subset <- mcq.plot.peptide.intensity(normalized_filtered_xic, rCutoff = 0.5, nprot = 10, getProtlist = TRUE)
	mcq.plot.peptide.intensity(normalized_filtered_xic, rCutoff = 0.3, protlist = protein_subset, file="output/pep_cor3.pdf")
	mcq.plot.peptide.intensity(normalized_filtered_xic, rCutoff=0.5, protlist = protein_subset, file="output/pep_cor5.pdf")
	# Filter
	normalized_filtered_xic <- mcq.drop.uncorrelated.peptides(normalized_filtered_xic, rCutoff=0.3)

############# Filter for proteins quantified by a small number of peptides-mz. 
	normalized_filtered_xic <- mcq.drop.proteins.with.few.peptides(normalized_filtered_xic, npep=2)

############# Display of a summary of the filtered data.
	summary(normalized_filtered_xic)

#********************************************************
#--------------------------------------------------------
#       2.7. Imputating missing values and aggregating peptide intensities at the protein level
#--------------------------------------------------------
#********************************************************

############# Impute missing peptide-mz intensities. No imputation is performed when a protein is completely absent from a sample.
	# With irmi
	irmi_imputed_xic <- mcq.compute.peptide.imputation(normalized_filtered_xic, method = "irmi")
	# With knn
	knn_imputed_xic <- mcq.compute.peptide.imputation(normalized_filtered_xic, method = "knn")

############# Check the imputed values.
	mcq.plot.peptide.intensity(irmi_imputed_xic, showImputed = TRUE , protlist = protein_subset, file="output/imputed_peptide_intensity_irmi.pdf")
	mcq.plot.peptide.intensity(knn_imputed_xic, showImputed = TRUE , protlist = protein_subset, file="output/imputed_peptide_intensity_knn.pdf")	

############# Aggregate peptide intensities to calculate protein abundances.
	prot_abund <- mcq.compute.protein.abundance(knn_imputed_xic, method="mean")

############# Impute missing protein abundances.
	imputed_prot_abund <- mcq.compute.protein.imputation(prot_abund)
	

#********************************************************
#--------------------------------------------------------
#       2.8. Descriptive analyses
#--------------------------------------------------------
#********************************************************

#############  PCA representation
	pca_on_proteins <- mcq.compute.pca(imputed_prot_abund, flist=c("genotype","environment", "replicate"))
	mcq.plot.pca(pca_on_proteins, factorToColor=c("genotype","environment"), labels =c("genotype","environment", "replicate"), tagType="label", labelSize=3, file="output/pca_prot.pdf")

############# Heatmap representation
	mcq.plot.heatmap(imputed_prot_abund, flist=c("genotype","environment", "replicate"), factorToColor=c("genotype", "environment"), protLab=FALSE, file="output/heatplot.pdf")

############# Distribution of the coefficients of variation	
	cv_prot <- mcq.compute.cv(imputed_prot_abund)
	mcq.plot.cv(cv_prot,file="output/cv.pdf" )
	
############# Perform protein clustering based on their expression profiles.
	# Compare different clustering methods.
	compare_protein_clusters <- mcq.compute.cluster(imputed_prot_abund, flist=c("genotype","environment"), nbclust=c(6:9), method=c("kmeans", "sota"))
	# Plot the results of one clustering method.	
	mcq.plot.cluster(compare_protein_clusters, method="kmeans", nbclust=9, file="output/cluster.pdf")

############# Boxplots of protein abundances.
	mcq.plot.protein.abundance(imputed_prot_abund, flist=c("genotype","environment"), factorToColor=c("genotype","environment"), labelprot="accession", file="output/boxplot_prot.pdf" )


#********************************************************
#--------------------------------------------------------
#       2.9. Hypothesis testing with ANOVA and Tukey test
#--------------------------------------------------------
#********************************************************

############# If desired, remove proteins showing low abundance variation. The selection criteria is the ratio between the minimum and the maximum abundance values. 
	imputed_filtered_prot_abund <- mcq.drop.low.fold.changes(imputed_prot_abund, cutoff=1.2, flist=c("genotype","environment"))

############# Perform an analysis of variance (ANOVA).
	anova_on_proteins <- mcq.compute.anova(imputed_prot_abund, flist=c("genotype","environment"))
	
############# Select proteins with non zero parameters for each factor in the ANOVA model.
	proteins_non_zero_geno <- mcq.select.pvalues(anova_on_proteins, padjust=TRUE, 0.05, flist="genotype")
	quanti_proteins_non_zero_geno <- mcq.select.from.protlist(imputed_filtered_prot_abund, proteins_non_zero_geno)
	
	proteins_non_zero_env <- mcq.select.pvalues(anova_on_proteins, padjust=TRUE, 0.05, flist="environment") 
	quanti_proteins_non_zero_env <- mcq.select.from.protlist(imputed_filtered_prot_abund, proteins_non_zero_env)
	
	proteins_non_zero_genoenv <- mcq.select.pvalues(anova_on_proteins, padjust=TRUE, 0.05, flist=c("genotype","environment"))
	quanti_proteins_non_zero_genoenv <- mcq.select.from.protlist(imputed_filtered_prot_abund, proteins_non_zero_genoenv)

############# Perform post-hoc Tukey tests
	tukey_geno <- mcq.compute.tukey(anova_on_proteins, flist="genotype", selected_list=proteins_non_zero_geno)
	tukey_env <- mcq.compute.tukey(anova_on_proteins, flist="environment", selected_list=proteins_non_zero_env)

#############  Graphical representations of the results from hypothesis testing 
	# Distribution of ANOVA p-values
	mcq.plot.pvalues(anova_on_proteins, file="output/pvalues.pdf")

	# Volcano plot
	mcq.plot.volcano(anova_on_proteins, file="output/volcano.pdf")	
	
	# Distribution of fold changes for each factor in the ANOVA
	geno_fc <- mcq.compute.fold.change(quanti_proteins_non_zero_geno, flist="genotype")
	mcq.plot.fold.change(geno_fc, file="output/geno_fold_change.pdf")
	
	env_fc <- mcq.compute.fold.change(quanti_proteins_non_zero_env, flist="environment")
	mcq.plot.fold.change(env_fc, file="output/environment_fold_change.pdf")
	
	# Display the results of the Tukey tests on boxplots of protein abundances.
	mcq.plot.tukey(tukey_geno, factorToColor="genotype", file="output/tukey_geno.pdf")
	mcq.plot.tukey(tukey_env, factorToColor="environment", file="output/tukey_env.pdf")

	# Plot a Venn diagram with proteins with a non zero parameter for the genotype and environment factors
	mcq.plot.venn(proteins_non_zero_geno, proteins_non_zero_env, protlistsNames=c("Genotype", "Environment"), file="output/venn.pdf")


#********************************************************
#--------------------------------------------------------
#       2.10. Exporting results
#--------------------------------------------------------
#********************************************************

############# Extract and export PCA results.
	pca_on_proteins_results <- mcq.get(pca_on_proteins)
	mcq.write(pca_on_proteins_results, file=("output/pca_on_proteins_results"))

############# Extract and export protein quantification data.
	protein_abundance_table <- mcq.get(imputed_filtered_prot_abund)
	mcq.write(protein_abundance_table, file="output/protein_abundance_table", type="ods")

############# Extract and export clustering results.    
	my_clustering <- mcq.compute.cluster(imputed_prot_abund, flist=c("genotype", "environment"), nbclust=c(9), method=c("kmeans"))   
	my_clustering_results <- mcq.get(my_clustering)
	mcq.write(my_clustering_results, file="output/xic_clustering_results", type="ods")

############# Extract and export ANOVA results.  
	my_anova_results <- mcq.get(anova_on_proteins)
   	mcq.write(my_anova_results, file="output/anova_results", type="ods")
   	
############# Extract and export fold changes. 
 	my_geno_fold_change <- mcq.get(geno_fc)
   	mcq.write(my_geno_fold_change, file="output/geno_fc", type="ods")

############# Extract and export a table merging ANOVA results with protein quantification data.
	merge_anova_quanti <- mcq.get.merge(object=imputed_prot_abund, object2=anova_on_proteins)
	mcq.write(merge_anova_quanti, file="output/merged_results", type="ods")


#**************************************************************************************************************************************************
#**************************************************************************************************************************************************
#******************************************************   3. Analyzing spectral counting (SC) data
#**************************************************************************************************************************************************
#**************************************************************************************************************************************************

#********************************************************
#--------------------------------------------------------
#       3.1. Loading data and metadata
#--------------------------------------------------------
#********************************************************

# Load spectral counting data.
	raw_sc <- mcq.read.spectral.counting("input/spectral_counts.tsv")	
	
# Check the content of the data.
	summary(raw_sc)	

# Export a template file for the metadata. The file will be saved in '/path/to/my_working_directory/output' in ODS format.
	mcq.write.metadata.template(raw_sc, file="output/metadata_template", type="ods")

#!!!!!!!!!!!!!!!!!!!!! Open the template file in Libreoffice or Excel, fill in the columns with all the information about your MS runs, and save it as a new ODS file. Here, the filled metadata is already available: '/path/to/my_working_directory/input/filled_metadata.ods'. You can also reuse the metadata used for the XIC data. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# Import the metadata.
	my_metadata <- mcq.read.metadata("input/filled_metadata.ods")
	
# Check the content of the metadata.
	summary(my_metadata)	

# Associate the metadata with the SC data.
	raw_sc <- mcq.add.metadata(raw_sc, metadata = my_metadata)



#********************************************************
#--------------------------------------------------------
#       3.2. Quality check (QC)
#--------------------------------------------------------
#********************************************************

############# Check the number of spectra for each MS run.
	mcq.plot.counts(raw_sc, file="output/SC_counts.pdf")


#********************************************************
#--------------------------------------------------------
#       3.3. Summarizing fractions to the sample level
#--------------------------------------------------------
#********************************************************

############# Compute protein abundances by track as the sum of spectral counts of all fractions for each track
	sc_by_track <- mcq.compute.quantity.by.track(raw_sc)
	summary(sc_by_track)

#############  Check the data and samples using a principal component analysis (PCA). 
	pca_on_raw_sc <- mcq.compute.pca(sc_by_track)
	mcq.plot.pca(pca_on_raw_sc, factorToColor=c("genotype", "environment"), labels = c("genotype", "environment"), tagType="label", file="output/SC_pca.pdf")

#********************************************************
#--------------------------------------------------------
#       3.4. Protein filtering
#--------------------------------------------------------
#********************************************************
	filtered_sc_by_track <- sc_by_track

#############  Remove low abundant proteins (i.e., those whose maximal SC is below the cuf-off).
	filtered_sc_by_track <- mcq.drop.low.counts(filtered_sc_by_track, cutoff=5)


#********************************************************
#--------------------------------------------------------
#       3.5. Descriptive analyses
#--------------------------------------------------------
#********************************************************

############# PCA representation
	pca_on_filt_sc <- mcq.compute.pca(filtered_sc_by_track, flist=c("genotype", "environment"))		
	mcq.plot.pca(pca_on_filt_sc, factorToColor=c("genotype", "environment"), tagType="label", labels=c("genotype", "environment"), file="output/SC_pca_prot.pdf" )

############# Distribution of the coefficients of variation	
	cv_prot <- mcq.compute.cv(filtered_sc_by_track)
	mcq.plot.cv(cv_prot,file="output/SC_cv.pdf" )

############# Heatmap represention
	mcq.plot.heatmap(filtered_sc_by_track, flist=c("genotype", "environment"), factorToColor=c("genotype", "environment"), protLab=FALSE, file="output/SC_heatmap.pdf")

############# Protein clustering
	SC_clusters <- mcq.compute.cluster(filtered_sc_by_track, flist=c("genotype", "environment"), nbclust=(6:9), method=c("kmeans", "sota"))
	mcq.plot.cluster(SC_clusters, method="kmeans", nbclust=6, file="output/SC_cluster.pdf")

############# Boxplots of protein abundances
	mcq.plot.protein.abundance(filtered_sc_by_track, flist=c("genotype", "environment"), factorToColor=c("genotype", "environment"), labelprot="accession", file="output/SC_boxplot_prot.pdf" )


#********************************************************
#--------------------------------------------------------
#       3.6. Hypothesis testing
#--------------------------------------------------------
#********************************************************

############# Remove the proteins already that have been already analyzed in the XIC-based data.
	prot_analyzed_xic <- mcq.get.protlist(imputed_prot_abund)
	sc_without_xic <- mcq.drop.from.protlist(filtered_sc_by_track, prot_analyzed_xic)

############# If desired, remove proteins showing little abundance variations. The selection criteria is the ratio between the minimum and the maximum abundance values. 
	sc_without_xic <- mcq.drop.low.fold.changes(sc_without_xic, cutoff=1.2, flist=c("genotype", "environment"))

############# Perform two-way ANOVA (but in that case Tukey test does not work
	anova_on_sc_proteins_2way <- mcq.compute.anova(sc_without_xic, flist=c("genotype", "environment"))
	
############# or perform way ANOVA by concatenating the 2 factors
	anova_on_sc_proteins_1way <- mcq.compute.anova(sc_without_xic, flist=c("geno_env"))

############# Select proteins with non-zero parameter for each factor in the ANOVA model.
	sc_proteins_non_zero_genotype <- mcq.select.pvalues(anova_on_sc_proteins_2way, padjust=TRUE, 0.05, flist="genotype")
	sc_quanti_proteins_non_zero_genotype <- mcq.select.from.protlist(sc_without_xic, sc_proteins_non_zero_genotype)
	
	sc_proteins_non_zero_environment <- mcq.select.pvalues(anova_on_sc_proteins_2way, padjust=TRUE, 0.05, flist="environment") 
	sc_quanti_proteins_non_zero_environment <- mcq.select.from.protlist(sc_without_xic, sc_proteins_non_zero_environment)
	
	sc_proteins_non_zero_genoenv <- mcq.select.pvalues(anova_on_sc_proteins_2way, padjust=TRUE, 0.05, flist=c("genotype", "environment"))
	sc_quanti_proteins_non_zero_genoenv <- mcq.select.from.protlist(sc_without_xic, sc_proteins_non_zero_genoenv)
	
	sc_proteins_non_zero_cond <- mcq.select.pvalues(anova_on_sc_proteins_1way, padjust=TRUE, 0.05, flist=c("geno_env"))
	sc_quanti_proteins_non_zero_cond <- mcq.select.from.protlist(sc_without_xic, sc_proteins_non_zero_cond)	

############# Perform post-hoc Tukey tests
	sc_tukey_cond <- mcq.compute.tukey(anova_on_sc_proteins_1way, flist="geno_env", selected_list=sc_proteins_non_zero_cond)


############# Graphical representations of the results from hypothesis testing 
	# Distribution of ANOVA p-values
	mcq.plot.pvalues(anova_on_sc_proteins_2way, file="output/SC_2way_pvalues.pdf")
	mcq.plot.pvalues(anova_on_sc_proteins_1way, file="output/SC_1way_pvalues.pdf")
	
	# Volcano plot
	mcq.plot.volcano(anova_on_sc_proteins_2way, file="output/SC_volcano.pdf")	
	
	# Distribution of fold changes for each factor in the ANOVA
	sc_environment_fc <- mcq.compute.fold.change(sc_quanti_proteins_non_zero_environment, flist="environment")
	mcq.plot.fold.change(sc_environment_fc, file="output/SC_environment_fold_change.pdf")

	# Display the results of the Tukey tests on boxplots of protein abundances.
	mcq.plot.tukey(sc_tukey_cond, factorToColor=c("geno_env"), file="output/SC_tukey.pdf")


#********************************************************
#--------------------------------------------------------
#       3.7. Exporting results
#--------------------------------------------------------
#********************************************************

############# Extract and export PCA results
	pca_on_sc <- mcq.get(pca_on_filt_sc)
	mcq.write(pca_on_sc, file=("output/SC_pca"))

############# Extract and export protein quantification data
	sc_table <- mcq.get(sc_without_xic)
	mcq.write(sc_table, file="output/spectral_count_table", type="ods")

############# Extract and export clustering results    
	my_clustering_sc <- mcq.compute.cluster(filtered_sc_by_track, flist=c("genotype", "environment"), nbclust=(6), method=c("kmeans"))
	my_clustering_results_sc <- mcq.get(my_clustering_sc)
	mcq.write( my_clustering_results_sc, file="output/SC_clustering_results", type="ods")

############# Extract and export ANOVA results  
	my_anova_results_sc <- mcq.get(anova_on_sc_proteins_2way)
   	mcq.write(my_anova_results_sc, file="output/SC_my_anova_results", type="ods")
   	
############# Extract and export fold changes 
 	my_environment_fold_change_sc <- mcq.get(sc_environment_fc )
   	mcq.write(my_environment_fold_change_sc, file="output/SC_environment_fc_sc", type="ods")

############# Extract and export a table merging ANOVA results with protein quantification data
	merge_anova_quanti_sc <- mcq.get.merge(object=sc_without_xic, object2=anova_on_sc_proteins_2way)
	mcq.write(merge_anova_quanti_sc, file="output/SC_merged_results", type="ods")






