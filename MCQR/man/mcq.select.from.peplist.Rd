\name{mcq.select.from.peplist}

\alias{mcq.select.from.peplist}

\title{Select a Subset of Data from a List of Peptides-mz}

\description{This function selects the subset of data corresponding to a given list of peptides-mz.}

\usage{mcq.select.from.peplist(object, peplist=NULL)}

\arguments{
	\item{object}{an object of class 'cmcq.pepq'.}
	\item{peplist}{an object of class 'cmcq.peplist'.}
}


\value{'mcq.select.from.peplist' returns an object of the same class as the input object.}


\author{PAPPSO (pappso-projetsbioinfo@groupes.renater.fr)}





