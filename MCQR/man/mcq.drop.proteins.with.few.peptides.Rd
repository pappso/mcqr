\name{mcq.drop.proteins.with.few.peptides}

\alias{mcq.drop.proteins.with.few.peptides}

\title{Delete Proteins Quantified by Few Peptides-mz}

\description{This function removes proteins quantified by fewer peptides-mz than a given threshold from the proteomic dataset.}

\usage{mcq.drop.proteins.with.few.peptides(object, npep=2)}

\arguments{
	\item{object}{an object of class 'cmcq.pepq.masschroq' or 'cmcq.pepq.by.track'.}
	\item{npep}{a numeric value indicating the threshold for the number of peptides-mz per protein.}
}


\value{'mcq.drop.proteins.with.few.peptides' returns an object of the same class as the input object.}


\author{PAPPSO (pappso-projetsbioinfo@groupes.renater.fr)}



\examples{

# Load the example dataset 
	data(xic_imputed)
	
# Remove the proteins quantified by less than 3 peptides
	xic_filtered=mcq.drop.proteins.with.few.peptides(xic_imputed, npep=3)

# Check the effects of the filter
	summary(xic_imputed)
	summary(xic_filtered)
}

