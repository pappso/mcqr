\name{mcq.plot.fold.change}

\alias{mcq.plot.fold.change}

\title{Plot Protein Abundance Fold Change}

\description{This function plots protein abundance fold changes}

\usage{mcq.plot.fold.change(object, file=NULL, labelprot="accession")}

\arguments{
	\item{object}{an object of class 'cmcq.ratio'.}
	\item{file}{a character string indicating the name of the PDF file where to export the graphs (e.g. "mygraph.pdf"). By default, the graphs are displayed on the screen.}
	\item{labelprot}{a character string indicating the protein name to use. Must be "accession" (default) or "description".}
}


\value{The function 'mcq.plot.fold.change' returns a graph that is displayed on the screen when the file argument is NULL or that is exported as a PDF file when the file argument is specified.}


\author{PAPPSO (pappso-projetsbioinfo@groupes.renater.fr)}

\note{'mcq.plot.fold.change' calls 'ggplot' in the 'ggplot2' package.}

\seealso{'mcq.compute.fold.change'}

\examples{
# Load the example dataset
	data(xic_prot_imputed)

# Compute the abundance fold change between two levels
	my_fold_change <- mcq.compute.fold.change(xic_prot_imputed,
            flist="time",
            numerator="36H",
            denominator="48H")
	mcq.plot.fold.change(my_fold_change) # to see the fold changes

# Compute the max fold change for a given factor, regardless the level considered
# (in that case, the factor "time" has 3 different levels)
	my_fold_change <- mcq.compute.fold.change(xic_prot_imputed, flist="time")
	mcq.plot.fold.change(my_fold_change)

}
