\name{mcq.drop.shared.peptides}

\alias{mcq.drop.shared.peptides}

\title{Delete Peptides Shared Between Proteins}

\description{This function removes the peptides shared by two or more sub-groups of proteins from the proteomic dataset.}

\usage{mcq.drop.shared.peptides(object)}

\arguments{
	\item{object}{an object of class 'cmcq.pepq'.}
}

\details{This function retains peptides associated with only one protein sub-group as defined by X!Tandempipeline (Langella et al., 2016). These peptides are not necessarily proteotypic peptides.}

\value{'mcq.drop.shared.peptides' returns an object of the same class as the input object.}

\references{Langella et al. (2016) X!TandemPipeline: A tool to manage sequence redundancy for protein inference and phosphosite identification. J. Prot. Res. }

\author{PAPPSO (pappso-projetsbioinfo@groupes.renater.fr)}


\seealso{'mcq.plot.peptide.sharing'}

\examples{

# Load the example dataset 
	data(xic_raw)
	
# Remove shared peptides
	xic_filtered <- mcq.drop.shared.peptides(xic_raw)
	summary(xic_raw)
	summary(xic_filtered)
}

