\name{xic_raw}
\alias{xic_raw}
\docType{data}
\title{Example MCQR object containing raw peptide intensity data}

\description{
	xic_raw is an object of class 'cmcq.pepq.masschroq' obtained from raw peptide intensity data produced by MassChroq. This object is used as an example to illustrate MCQR functions.}

\usage{data(xic_raw)}

\format{xic_raw is an object of class 'cmcq.pepq.masschroq'.}

\details{xic_raw was obtained after executing the 'mcq.read.masschroq' and 'mcq.add.metadata' functions. The data set contained in xic_raw is a subset of the data described and analyzed in Millan-Oropeza et al. 2017.}

\references{
	Millan-Oropeza A, Henry C, Blein-Nicolas M, Aubert-Frambourg A, Moussa F, Bleton J, Virolle MJ. (2017)
 Quantitative Proteomics Analysis Confirmed Oxidative Metabolism Predominates in Streptomyces coelicolor versus Glycolytic Metabolism in Streptomyces lividans.
 J. Proteome Res., 7 (16) 2597-2613.
}

\examples{
	data(xic_raw)
	summary(xic_raw)
}

\keyword{datasets}
