\name{mcq.compute.anova}

\alias{mcq.compute.anova}


\title{Analysis of Variance}

\description{This function fits a linear model on protein quantification data and performs an analysis of variance.}

\usage{mcq.compute.anova(object,
						 flist=NULL,
						 interaction=TRUE,
						 warningGLM="Warnings_anova_GLM.txt")}

\arguments{
	\item{object}{an object of class 'cmcq.protq'.}
	\item{flist}{a character vector indicating the factor(s) to include in the linear model. A maximum of two factors are allowed.}
	\item{interaction}{a boolean indicating whether an interaction term should be included in the model (valid only if there are two factors).}
	\item{warningGLM}{a file to save the warning messages that occur during computation, used only for 'cmcq.protq.spectralcounting', 'cmcq.protq.by.track' and 'cmcq.protq.peakcounting' object without normalization.}
}
\details{When the input object is of class 'cmcq.protq.xic', 'mcq.compute.anova' fits a simple linear model with fixed effects. When the input object is of class 'cmcq.protq.peakcounting', or 'cmcq.protq.spectralcounting', 'mcq.compute.anova' fits a generalized linear model with a Poisson distribution. In that case, a deviance goodness-of-fit test is performed with a p-value threshold at 0.01. Warnings produced during the computation of generalized linear model fitting are redirected to the file specified by the 'warningGLM' argument.}

\value{'mcq.compute.anova' returns an object of class 'cmcq.anova'.}


\author{PAPPSO (pappso-projetsbioinfo@groupes.renater.fr)}

\note{Type III ANOVA is performed using 'Anova' from 'car' package.}

\seealso{'mcq.plot.pvalue', 'mcq.get'}

\examples{
# Load the example dataset
	data(xic_prot_imputed)

# Execute 'mcq.compute.anova'
	my_anova <- mcq.compute.anova(xic_prot_imputed, flist=c("time", "strain"), interaction=TRUE)
	mcq.plot.pvalues(my_anova) # to check pvalue distribution

# Extract ANOVA results in a dataframe
	my_anova_results <- mcq.get(my_anova)
	head(my_anova_results)

}
