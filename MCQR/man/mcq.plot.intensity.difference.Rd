\name{mcq.plot.intensity.difference}

\alias{mcq.plot.intensity.difference}

\title{Plot Log-Intensity Difference}

\description{This function plot a histogram of the distribution of the differences of peptide log10-intensity between a given MS run/track/factor level and a reference MS run/track/factor level.}

\usage{mcq.plot.intensity.difference(object, ref=NULL, width=0.1, ncol=2, nrow=2, file=NULL)
}

\arguments{
	\item{object}{an object of class 'cmcq.pepq.masschroq', 'cmcq.pepq.by.track', or 'cmcq.pepq.by.factor'.}
	\item{ref}{a character string indicating the name of the reference (name of a MS run, track, or factor level depending on the class of the input object).}
	\item{width}{a numeric indicating the width of the bars. By default, width=0.1.}
	\item{ncol}{an integrer indicating the number of graph panels in columns.}
	\item{nrow}{an integrer indicating the number of graph panels in rows.}
	\item{file}{a character string indicating the name of the PDF file where to export the histograms (e.g. "mygraph.pdf"). By default, the graphs are displayed on the screen.}

}

\details{Intensity difference between is computed on log-transformed intensities. 'ref' should be a MS run, a track, or a factor level for objects of class 'cmcq.pepq.masschroq', 'cmcq.pepq.by.track', and 'cmcq.pepq.by.factor', respectively.}

\value{'mcq.plot.intensity.difference' returns a series of graphs that are displayed on the screen when the file argument is NULL or that are exported as a PDF file when the file argument is specified.}


\author{PAPPSO (pappso-projetsbioinfo@groupes.renater.fr)}

\note{'mcq.plot.intensity.difference' calls 'ggplot' in the 'ggplot2' package.}



\examples{
# Load the example dataset
	data(xic_raw)

# Execute 'mcq.plot.intensity.difference'
	mcq.plot.intensity.difference(xic_raw, ref="msruna6")
	mcq.plot.intensity.difference(xic_raw, ref="msrunb12", width=1, ncol=3)

}
