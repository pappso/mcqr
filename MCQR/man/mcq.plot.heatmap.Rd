\name{mcq.plot.heatmap}

\alias{mcq.plot.heatmap}

\title{Plot Heatmap}

\description{This function plots a heatmap, i.e,. a color-coded representation of protein relative abundance with dendrograms on the left and top showing the hierarchical clustering of proteins and MS runs or factors, respectively.}

\usage{
	mcq.plot.heatmap(
		object,
		flist=NULL,
		factorToColor=NULL,
		distfun="1-pearson",
		method="ave",
		file=NULL,
		cexCol=0.8,
		cexRow=0.2,
		col="RdBu",
		getProtlist=FALSE,
		protLab=TRUE,
		labelprot="accession")}

\arguments{
	\item{object}{an object of class 'cmcq.protq.xic', 'cmcq.protq.spectralcounting', 'cmcq.protq.peakcounting', or 'cmcq.pepq.ratio.with.ref'.}
	\item{flist}{a character vector indicating a factor or a combination of factors on which to compute the mean of protein abundance.}
	\item{factorToColor}{a character vector indicating which factor or combination of factors to color in a colorbar associated to the columns.}
	\item{distfun}{a character string indicating the function to use to compute the distance between both rows and columns. May be one of "1-pearson", "euclidean", "maximum", "manhattan", "canberra", "binary" or "minkowski". Default is "1- Pearson".}
	\item{method}{a character string indicating the agglomeration method to be used. This should be one of "ward.D", "single","complete", "average", "mcquitty", "median" or "centroid". Default is "ave". }
	\item{file}{a character string indicating the name of the PDF file to be exported.}
	\item{cexCol}{a numerical value giving the amount by which column labels should be magnified relative to default.}
	\item{cexRow}{a numerical value giving the amount by which row labels  should be magnified relative to default.}
	\item{col}{a character string indicating which color panel to use. May be one of "BrBG", "PiYG", "PRGn", "PuOr", "RdBu", "RdGy", "RdYlBu", "RdYlGn", "RdBkGn" or "Spectral". Default is "RdBu".}
	\item{getProtlist}{a boolean indicating whether to return an object of class 'cmcq.protlist' containing protein IDs ordered as they appear in rows (from top to bottom). Default is FALSE.}
	\item{protLab}{a boolean indicating whether to display the protein IDs at the right of the heatmap. Default is TRUE.}
	\item{labelprot}{a character string indicating which protein IDs should be used. Must be chosen among "accession" (default) or "description".}
}


\value{'mcq.plot.heatmap' returns a heatmap with dendrograms of hierarchical cluster analysis. The figure is displayed on the screen when the file argument is NULL or is exported as a PDF file when the file argument is specified.}


\author{PAPPSO (pappso-projetsbioinfo@groupes.renater.fr)}

\note{'mcq.plot.heatmap' calls 'heatmap.2' in the 'gplots' package.}


\seealso{'dendrogram', 'hclust' and 'dist' in the 'stats' package.}

\examples{
# Preliminary step: load the example dataset and compute protein abundances
	data(xic_prot_imputed)

# Execute 'mcq.plot.heatmap'
	mcq.plot.heatmap(xic_prot_imputed, flist=c("time","rep"), factorToColor="time")

# Change the 'distfun' argument
	mcq.plot.heatmap(xic_prot_imputed,
									flist=c("time","rep"),
									factorToColor="time",
									distfun="euclidean")

# Change the 'method' argument
	mcq.plot.heatmap(xic_prot_imputed,
									flist=c("time","rep"),
									factorToColor="time",
									method="ward.D")

# Export the list of proteins ordered from top to bottom
	my_heatmap_proteins <- mcq.plot.heatmap(
		xic_prot_imputed,
		flist=c("time","rep"),
		factorToColor="time",
		getProtlist=TRUE)
	str(my_heatmap_proteins)
}
