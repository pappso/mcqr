\name{mcq.plot.rt.variability}

\alias{mcq.plot.rt.variability}

\title{Plot Peptide Retention Time Variability}

\description{This function plots a histogram of the distribution of the standard deviations of peptides-mz retention times.}

\usage{
	mcq.plot.rt.variability(
		object,
		file=NULL,
		limit = NULL,
		width=5,
		adjust=1,
		byLabel=FALSE)}

\arguments{
	\item{object}{an object of class 'cmcq.pepq.masschroq' or 'cmcq.pepq.masschroq.labeling'.}
	\item{file}{a character vector indicating the name of the PDF file where to export the graph (e.g. "mygraph.pdf"). By default, the graph is displayed on the screen.}
	\item{limit}{a numerical value indicating the upper bound of the x-axis.}
	\item{width}{a numerical indicating the width of the bars (valid for a histogram).}
	\item{adjust}{a numerical indicating a multiplicate bandwidth adjustment (valid for a density plot).}
	\item{byLabel}{a boolean indicating whether to plot one graph per labeling level or one graph for the whole experiment (valid only for labeling experiments).}
}

\details{The standard deviation of retention time is computed for each peptide-mz in the input object. In the case of fractionation experiments, the standard deviation is calculated independently for each fraction because retention time alignment is carried out separately for each fraction.}

\value{'mcq.plot.rt.variability' returns a graph that is displayed on the screen when the file argument is NULL or that is exported as a PDF file when the file argument is specified.}


\author{PAPPSO (pappso-projetsbioinfo@groupes.renater.fr)}

\note{'mcq.plot.rt.variability' calls 'ggplot' in the 'ggplot2' package.}


\examples{
# Load the example dataset
	data(xic_raw)

# Execute 'mcq.plot.rt.variability' 
	mcq.plot.rt.variability(xic_raw)
	mcq.plot.rt.variability(xic_raw, limit=250) # change the upper bound of the x-axis
	mcq.plot.rt.variability(xic_raw, limit=100, width=5)
}

