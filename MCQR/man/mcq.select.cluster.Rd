\name{mcq.select.cluster}
\docType{methods}

\alias{mcq.select.cluster}
\alias{mcq.select.cluster-methods}
\alias{mcq.select.cluster,cmcq.cluster-method}

\title{Select clustering}

\description{This function selects the results of a specific protein clustering method from those stored in a 'cmcq.cluster' object.}

\usage{
  mcq.select.cluster(object, method=NULL, nbclust=NULL)
}

\arguments{
  \item{object}{an object of class 'cmcq.cluster'.}
  \item{method}{one clustering method from those used to generate the input object. If method = NULL and the input object contains only one method, it will be automatically selected. Otherwise, the function stops.}
  \item{nbclust}{a number of clusters from those used to generate the input object. If nbclust=NULL and the input object contains only one number of cluster, it will be automatically selected. Otherwise, the function stops.}
}


\value{'mcq.select.cluster' returns an object of the same class as the input object. }


\author{PAPPSO (pappso-projetsbioinfo@groupes.renater.fr)}



\examples{
  \dontrun{
  # Load the example dataset
      data(xic_prot_imputed)

  # Execute 'mcq.compute.cluster' with 2 methods and 2 cluster numbers
      my_clusters <- mcq.compute.cluster(xic_prot_imputed, nbclust=c(7,8),
                                      method=c("kmeans", "sota"))

  # Select one clustering among the 4 stored into my_cluster
      my_selected_cluster <- mcq.select.cluster(my_clusters, method="kmeans", nbclust=7)
  }
}
