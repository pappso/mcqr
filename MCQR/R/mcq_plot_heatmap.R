mcq.plot.heatmap= function (object, flist=NULL, factorToColor=NULL, distfun="1-pearson", method="ave", file=NULL, cexCol=0.8, cexRow=0.2, col="RdBu", getProtlist=FALSE, protLab=TRUE, labelprot="accession") {
	# check the validity of object and flist
	if (!priv.mcq.is.protquant.object(object) & !is(object, "cmcq.pepq.ratio.with.ref")) {
		stop(paste("the class ", class(object)," is not taken into account by mcq.plot.heatmap.\n",sep=""))
	}
	if(is(object, "cmcq.protq.by.factor")){
		stop(paste("the class ", class(object)," is not taken into account by mcq.plot.heatmap.\n",sep=""))
	}

	if (nrow(object@proteins)==0) {
		stop(paste("there is no data in your object ",class(object),sep=""))
	}
	
	if (!is.null(flist)) {
		for (factor in flist) {
			if(!factor %in% names(object@metadata@metadata)) {
				stop(paste("factor ", factor, " is not described in your metadata\n", "available factors are : ", paste(colnames(object@metadata@metadata),collapse=", "), "\n", sep=""))
			}
		}
	}

	# check the validity of factorToColor
	if (!is.null(factorToColor)) {
		if((length(factorToColor) > length(flist)) & (length(flist)>0)){
			stop(paste("Too many factors chosen for factorToColor (", factorToColor, "), please choose one in: ", flist,"\n",sep=""))
		}

		if((length(factorToColor)>0) & (length(which(!factorToColor %in% flist))>0)){
			stop(paste("The factors chosen for factorToColor are not in agreement with those selected for flist.\n"))
		}
		for (color in factorToColor){
			if(!color %in% colnames(object@metadata@metadata)) {
				stop(paste("The factor ", color, " chosen for factorToColor is not described in your metadata\n", "please choose one in: ", paste(colnames(object@metadata@metadata),collapse=", "), "\n", sep=""))
			}
		}
	}

	# check the validity of col and distfun
	if(!col %in% c("BrBG", "PiYG", "PRGn", "PuOr", "RdBu", "RdGy", "RdYlBu", "RdYlGn", "RdBkGn", "Spectral")){
		stop("Uncorrect col argument. Please choose in: BrBG, PiYG, PRGn, PuOr, RdBu, RdGy, RdYlBu, RdYlGn, RdBkGn or Spectral.\n")
	}
	if(!distfun %in% c("1-pearson", "euclidean", "maximum", "manhattan", "canberra", "binary", "minkowski")){
		stop("Uncorrect distfun argument. Please choose in: 1-pearson, euclidean, maximum, manhattan, canberra, binary or minkowski.\n")
	}
	
	# check the validity of labelprot
	if(labelprot!="accession" && labelprot!="description"){
			stop("The 'labelprot' argument must be chosen among  'accession' or 'description'.\n")
	}

	# create the data frame for heatmap
	if(priv.mcq.is.protquant.object(object)){
		tprot <- merge(object@protval, object@metadata@metadata, by.x="msrun", by.y="row.names")
		tprot <- merge(tprot, object@metadata@injections, by.x="msrun", by.y="row.names")
		tprot <- merge(tprot, object@proteins, by="accession")

		if (is.null(flist)){
			tacp <- tapply (tprot$q, list(tprot$accession,tprot$msrunfile), FUN=mean)
			dim1 <- nrow(tacp)
			tacp <- na.omit(tacp)
			dim2 <- nrow(tacp)
		}else{
			if(is(object, "cmcq.protq.xic")){
				tprot$q <- 10^tprot$q
			}
			if(length(flist)==1){
				tprot$echantillon=tprot[, flist]
				tacp <- tapply (tprot$q, list(tprot$accession,tprot$echantillon), FUN=mean, na.rm=TRUE)
				if(is(object, "cmcq.protq.xic")){
					tacp <- log10(tacp)
				}
				dim1 <- nrow(tacp)
				tacp <- na.omit(tacp)
				dim2 <- nrow(tacp)
			}
			if(length(flist)>1){
				tprot$echantillon <- apply(tprot[,colnames(tprot) %in% flist], 1, paste, collapse="-")
				tacp <- tapply (tprot$q, list(tprot$accession,tprot$echantillon), FUN=mean, na.rm=TRUE)
				if(is(object, "cmcq.protq.xic")){
					tacp <- log10(tacp)
				}
	 			dim1 <- nrow(tacp)
				tacp <- na.omit(tacp)
				dim2 <- nrow(tacp)
				tacp <- tacp[,order(colnames(tacp))]
			}
		}
		
		if (dim1!=dim2){
		warning(paste("Your dataset contains ", dim1-dim2, " out of ", dim1, " proteins that are completely missing in some injections or in some levels of your flist. These proteins are not represented on the heatmap.\n", sep=""))
		}
	}
	
	if(is(object, "cmcq.pepq.ratio.with.ref")){
		if (!"logratio.norm" %in% colnames(object@peptides)) {
			stop("The intentity data have not been normalized")
		}
		
		object@metadata@metadata$msrun_label <- rownames(object@metadata@metadata)
		tprot <- object@peptides[,c("peptiz", "msrun_label","logratio.norm")]
		tprot <- merge(tprot, object@metadata@metadata, by="msrun_label")
		
		if (is.null(flist)){
			stop("Please indicate a list of factor in the 'flist' argument")			
		}else{
			if(length(flist)==1){
				tprot$echantillon=tprot[, flist]
				tacp <- tapply (tprot$logratio.norm, list(tprot$peptiz,tprot$echantillon), FUN=mean, na.rm=TRUE)
				dim1 <- nrow(tacp)
				tacp <- na.omit(tacp)
				dim2 <- nrow(tacp)
			}
			if(length(flist)>1){
				tprot$echantillon <- apply( tprot[ , colnames(tprot) %in% flist] , 1 , paste , collapse = "-" )
				tacp <- tapply(tprot$logratio.norm,list(tprot$peptiz,tprot$echantillon),FUN=mean)
	 			dim1 <- nrow(tacp)
				tacp <- na.omit(tacp)
				dim2 <- nrow(tacp)
				tacp <- tacp[,order(colnames(tacp))]
			}
		}
		
		if (dim1!=dim2){
		warning(paste("Your dataset contains ", dim1-dim2, " out of ", dim1, " peptides-mz that are completely missing in some injections or in some levels of your flist. These peptides-mz are not represented on the heatmap.\n", sep=""))
		}
	}
	
	# control variability
	ind <- apply(tacp, 1, var) == 0
	tacp <- tacp[!ind,]
	
	if(length(which(ind==TRUE))>0){
		warning(paste("Your dataset contains ",length(which(ind==TRUE))," out of ", dim1, " proteins that have no variability.These proteins are not represented on the heatmap.\n", sep=""))
	}
	
	ech <- apply(tacp, 2, var) == 0
	tacp <- tacp[,!ech]
	
	if(length(which(ech==TRUE))>0){
		warning(paste("Your dataset contains ",length(which(ech==TRUE))," out of ", length(ech), " injections that have no variability.These injections are not represented on the heatmap.\n", sep=""))
	}


	# create a vector of colors

	if (is.null(factorToColor)){
		ColSideColors <- NULL
	}else{
		mypal=c("#E41A1C","#0000FF","#00FF00","#FFA500","#E7298A","#000080","#00FFFF","#008000","#999999","#800080","#008080","#000000","#FF4500","#FFFF00","#4DAF4A","#800000","#A65628","#F781BF","#1B9E77","#808000","#377EB8")

		if(length(factorToColor)==1){
			temp.tab <- unique(tprot[,c("echantillon", flist)])
			temp.tab <- temp.tab[order(temp.tab$echantillon),]
			temp.vec <- temp.tab[,factorToColor]	
			colclass <- mypal[1: length(unique(temp.vec))]
			ColSideColors <- factor(temp.vec, labels = colclass)
		}
		if(length(factorToColor)>1){
			temp.tab <- unique(tprot[,c("echantillon", flist)])
			temp.tab <- temp.tab[order(temp.tab$echantillon),]
			temp.vec <- apply(temp.tab[,colnames(temp.tab) %in% factorToColor], 1, paste, collapse="-")	
			colclass <- mypal[1: length(unique(temp.vec))]
			ColSideColors <- factor(temp.vec, labels = colclass)
		}
	}

	# scale
	tacp <- t(scale(t(tacp)))

	# define colors
	
	if (col=="RdBkGn") {
		mypalette <- colorRampPalette(c("#59ff00", "black", "#FE1B00"))(n = 50)
	}else{
		mypalette <- rev(colorRampPalette(RColorBrewer::brewer.pal(11, col))(100))
	}

	# compute distance
	if (distfun=="1-pearson"){
 		distance <- function(x) as.dist(1 - cor(x, use ="pairwise.complete.obs" ))
	}else{
		distance <- function(x) dist(t(x), method = distfun)
	}

	# compute dendrogram
	Colv <- as.dendrogram(hclust(distance(tacp), method = method))
	Rowv <- as.dendrogram(hclust(distance(t(tacp)), method = method))

	#labRow
	if (protLab==FALSE){
		labRow <- ""
	}else{
		if(priv.mcq.is.protquant.object(object)){
			if (labelprot=="accession") {
				labRow <- NULL
			} else {
				labRow <- merge(tacp,object@proteins,by.x="row.names",by.y="accession")
				labRow <- labRow$descr
			}
		}
		if (is(object, "cmcq.pepq.ratio.with.ref")){
			labRow <- NULL
		}
	}

	# draw heatmap 
	if (!is.null(file)){
		pdf(file, title=gsub('.pdf$','',file))
	}
	
	if (is.null(ColSideColors)){
		gplots::heatmap.2(tacp,  Colv = Colv,  Rowv = Rowv, col=mypalette, scale="none", trace="none" , density.info = "none", dendrogram ="both", cexCol=cexCol, cexRow=cexRow, labRow=labRow)
	}else{
		gplots::heatmap.2(tacp,  Colv = Colv,  Rowv = Rowv, col=mypalette, scale="none", trace="none" , density.info = "none", dendrogram ="both", ColSideColors=as.character(ColSideColors), cexCol=cexCol, cexRow=cexRow, labRow=labRow)
	}
	
	if (!is.null(file)){
		msg <- dev.off()
		cat("The pdf file has been exported\n")
	}

	# create protein list
	if (priv.mcq.is.protquant.object(object) & getProtlist==TRUE){
		rowInd <- rev(order.dendrogram(Rowv))
		protlist <- merge(tacp,object@proteins,by.x="row.names",by.y="accession")
		colnames(protlist)[1] <- "accession"
		protlist <- protlist[rowInd,c('accession','descr')]
		proteinvector <- protlist$descr
		names(proteinvector) <- protlist$accession
		newObject <- new("cmcq.protlist",proteins=proteinvector)
		return(newObject)
	}else if (is(object, "cmcq.pepq.ratio.with.ref") & getProtlist==TRUE){
		print("The 'getProtlist' argument does not work with an object of class 'cmcq.pepq.ratio.with.ref'")
	}


}


