
mcq.plot.protein.abundance = function (object, protlist=NULL, flist=NULL, factorToColor=NULL, ncol=2, nrow=2, labelprot="accession", log=TRUE, file=NULL) {
  ## adding colnames used in data.table or ggplot to silencing R CMD check global variable
	factorName <- NULL
	if (priv.mcq.is.protquant.object(object)) {

		if (is.null(protlist)){
			protlist <- mcq.get.protlist(object)
		}else{
			if(!is(protlist, "cmcq.protlist")){
				stop("The 'protlist' argument is not of class 'cmcq.protlist'.")
			}else{
			object=mcq.select.from.protlist(object, protlist)
			}
		}

		if(labelprot!="accession"&& labelprot!="description"){
			stop("The 'labelprot' argument must be chosen among  'accession' or 'description'.")
		}

		if (sum(!flist %in% colnames(object@metadata@metadata))>0) {
			stop(paste("The factor '", flist[!flist %in% colnames(object@metadata@metadata)], "' is not in your metadata.", "Choose the factor or the combination of factors to display among: ", paste(colnames(object@metadata@metadata), collapse=", "),sep=""))
		}

		# control parameters : factorToColor
		if (is.null(factorToColor)) {
		}else{
			if((length(factorToColor) > length(flist)) & (length(flist)>0)){
				 stop(paste("Too many factors chosen for the 'factorToColor' argument (", factorToColor, "), please choose one in: ", flist,"\n",sep=""))
			}

			if((length(flist)>0) & (length(which(!factorToColor %in% flist))>0)){
				stop(paste("The factors chosen for the 'factorToColor' argument are not in agreement with those selected for the 'flist' argument."))
			}
			for (color in factorToColor){
				if(color %in% colnames(object@metadata@metadata)) {
				}else {
					stop(paste("The factor ", color, " chosen for the 'factorToColor' argument is not described in your metadata\n", "please choose one in: ", paste(colnames(object@metadata@metadata),collapse=", "), "\n", sep=""))
				}
			}
		}

		# data
		tabprot <- merge(object@protval, object@metadata@metadata, by.x="msrun", by.y="row.names")
		tabprot <- merge(tabprot, object@proteins, by="accession")


		if (is.null(flist)){
			tabprot$factorName=tabprot$msrun
		}
		if (length(flist)==1){
			tabprot$factorName=tabprot[,flist]
		}
		if (length(flist)>1){
			tabprot$factorName=paste(tabprot[,flist[1]], tabprot[,flist[2]], sep="-")
			if (length(flist)>2){
				for (i in 3:length(flist)){
					tabprot$factorName=paste(tabprot$factorName, tabprot[,flist[i]], sep="-")
				}
			}
		}

		if (is.null(factorToColor)) {
			tabprot$factorToColor="#56B4E9"
		}
		if (length(factorToColor)==1){
			tabprot$factorToColor=tabprot[,factorToColor]
		}
		if (length(factorToColor)>1){
			tabprot$factorToColor=paste(tabprot[,factorToColor[1]], tabprot[,factorToColor[2]], sep="-")
			if (length(factorToColor)>2){
				for (i in 3:length(factorToColor)){
					tabprot$factorToColor=paste(tabprot$factorToColor, tabprot[,factorToColor[i]], sep="-")
				}
			}
		}

		# if (!is.null(file)){
		# 	pdf(file, title=gsub('.pdf$','',file))
		# }


		# ylab
		if (is(object,"cmcq.protq.spectralcounting")) ylab <- "Number of spectra"
		if (is(object,"cmcq.protq.by.track")) ylab <- "Number of spectra"
		if (is(object,"cmcq.protq.peakcounting")) ylab <- "Number of chromatographic peaks"
		if (is(object,"cmcq.protq.xic") & log==TRUE) ylab <- "Abundance in log10"
		if (is(object,"cmcq.protq.xic") & log==FALSE){
			ylab <- "Abundance"
			tabprot$q <- 10^(tabprot$q)
		}

		# graph
		nbgraph <- ncol * nrow
		i <- 1
		prot <- unique(tabprot$protein)
		p = list()
		j=1
		while (i <= length(prot)){
			tabcour <- tabprot[tabprot$protein %in% prot[i:(i+nbgraph-1)],]
			if(is.numeric(tabcour$factorName)){
			  tabcour$factorName = as.character(tabcour$factorName)
			}
			temp <- ggplot(tabcour, aes(x=factorName, y=q)) + geom_boxplot(aes(color=factorToColor))+ labs(y=ylab)+ theme(axis.text.x = element_text(angle=90), axis.title.x=element_blank())+ theme(legend.title=element_blank())

			if (is.null(factorToColor)){
				temp <- temp+ theme(legend.position="none")
			}

			if(labelprot=="accession"){
				temp <- temp+facet_wrap(~accession, ncol=ncol, nrow=nrow, scales="free_y")
			}else{
				temp <- temp+facet_wrap(~descr, ncol=ncol, nrow=nrow, scales="free_y")
			}

			p[[j]]<- temp
			j <- j+1
			i <- i+nbgraph

			# if (is.null(file)){
			# 	par(ask=TRUE)
			# }

		}

		# if (!is.null(file)){
		# 	dev.off()
		# }
		priv.mcq.visualisation.graph(p, file)

	}else{
		stop(paste("The class  ", class(object), " of the input object is not taken into account by mcq.plot.protein.abundance", sep=""))
	}
}
