
mcq.plot.intensity.difference<- function (object, ref=NULL, width=0.1, ncol=2, nrow=2, file=NULL) {
	if (((is(object, "cmcq.pepq.masschroq")) && !object@is_fraction) || (is(object, "cmcq.pepq.skyline")) || (is(object, "cmcq.pepq.by.track")) || (is(object, "cmcq.pepq.by.factor"))) {
		if (!ref %in% object@peptides$msrun) {
			stop(paste("The reference ",ref," is not in the input object of class ",class(object), sep=""))
		}

		if("lognorm" %in% colnames(object@peptides)) {
			object@peptides$q <- object@peptides$lognorm
		}else{
			object@peptides$q <- object@peptides$logarea
			cat(paste("Info : Intensities are not normalized in the input object of class.", class(object) , "\n", sep=""))
		}

		tab.ref <- object@peptides[object@peptides$msrun==ref,]
		indice <- match(object@peptides$peptiz, tab.ref$peptiz)
		object@peptides$q_ref=tab.ref$q[indice]
		object@peptides$diff=object@peptides$q - object@peptides$q_ref
		object@peptides$title=paste(object@peptides$msrun, " - ", ref, sep="")


		nbgraph <- ncol*nrow
		i <- 1
		msrun <- unique(object@peptides$msrun)
		msrun=msrun[order(msrun)]
		msrun=msrun[-which(msrun==ref)]
		if (!is.null(file)){
			pdf(file)
		}

		while (i <= length(msrun)){
			tabcour <- object@peptides[object@peptides$msrun %in% msrun[i:(i+nbgraph-1)],]
			p <- ggplot(tabcour, aes(x=diff, color="red", fill="red"))+ geom_histogram(binwidth=width, alpha=0.6)+ labs(y="Number of peptides-mz", x="Difference of Log10(intensity)")+ theme(legend.position="none")+facet_wrap(~title, ncol=ncol, nrow=nrow)

			print(p)
			i <- i+nbgraph

			if (is.null(file)){
				devAskNewPage(ask=TRUE)
			}

		}

		if (!is.null(file)){
			msg <- dev.off()
			cat("The pdf file has been exported\n")
		}else{
			devAskNewPage(ask=FALSE)	
		}

	}else {
	  if((is(object, "cmcq.pepq.masschroq")) & object@is_fraction){
	    stop(paste("The class \"",class(object),"\" with fraction is not taken into account by the function mcq.plot.intensity.difference",sep=""))
	  }else{
	    stop(paste("the class \"", class(object),"\" is not taken into account by the function mcq.plot.intensity.difference",sep=""))
	  }
	}
}
