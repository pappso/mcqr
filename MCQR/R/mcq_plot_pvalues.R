
mcq.plot.pvalues <- function (object,  width=0.01, file = NULL) {
	## adding colnames used in data.table or ggplot to silencing R CMD check global variable
	values <- pvalue <- NULL
	if (priv.mcq.is.anova.object(object) || (is(object,"cmcq.ttest")) || (is(object,"cmcq.anova.contrast"))) {
	}else{
		stop(paste("The input object of class", class(object)," is not taken into account by mcq.plot.pvalues"))
	}
	p <- list()
	if (priv.mcq.is.anova.object(object)) {
	  i <- 1
		factor <- colnames(object@resultanov)[grep("pval",colnames(object@resultanov))]
		if(length(factor)==1){
			p[[i]] <- ggplot(object@resultanov, aes_string(factor))+geom_histogram(binwidth=width, color="#56B4E9", fill="white")+ labs(x="P-values", title=paste("Distribution of p-values for factor ", substring(factor, 6, nchar(factor)), sep=""))+theme(plot.title = element_text(hjust=0.5))
		}else{
		  j <- 1
		  nbGraph <- 4
		  tabcour <- stack(object@resultanov[,colnames(object@resultanov) %in% factor[seq(i:nbGraph+i)]])
		  temp <- ggplot(tabcour, aes(values))+geom_histogram(binwidth=width, color="#56B4E9", fill="white")+ labs(x="P-values")+facet_wrap(~ind, nrow=2)
		  p[[j]] <- temp
		  j <- j+1
		  i <- i+nbGraph
		  while(i < length(factor)){
		    tabcour <- stack(object@resultanov[,colnames(object@resultanov) %in% factor[seq(i:nbGraph+i)]])
		    temp <- ggplot(tabcour, aes(values))+geom_histogram(binwidth=width, color="#56B4E9", fill="white")+ labs(x="P-values")+facet_wrap(~ind, nrow=2)
		    p[[j]] <- temp
		    j <- j+1
		    i <- i+nbGraph
		  }
		}

	}else if (is(object,"cmcq.ttest")) {
	  p[[i]] <- ggplot(object@resultstudent, aes(pvalue))+geom_histogram(binwidth=width, color="#56B4E9", fill="white")+ labs(x="P-values", title=paste("Distribution of p-values for factor \"", object@paramlist$factor,"\"", sep=""))+theme(plot.title = element_text(hjust=0.5))
	}else if (is(object,"cmcq.anova.contrast")){
	  pvalues <- stack(data.table::as.data.table(object@pvalue.lm.adjust))
	  pvalues$ind <- as.character(pvalues$ind)
	  contrast <- object@paramlist$computedContrast
	  # print("****** in contrast ******")
	  # print(contrast)
	  # print("*************************")
	  i=1
# 	  print("**** Get for graph *******")
# 	  print(paste("Range for selection : ", i, " - ", nbGraph+1))
# 	  print(contrast[i:nbGraph+1])
# 	  print("**************************")
	  tabcour <- pvalues[pvalues$ind %in% contrast[i],]
	  temp <- ggplot(tabcour, aes(values))+geom_histogram(binwidth=width, color="#56B4E9", fill="white")+ labs(x="P-values")+ggtitle(unique(tabcour$ind))
	  p[[i]] <- temp
	  i <- i+1
	  while(i < length(contrast)+1){
# 	    print("**** Get for graph *******")
# 	    print(paste("Range for selection : ", i, " - ", i+nbGraph+1))
# 	    print(contrast[i:(i+nbGraph+1)])
# 	    print("**************************")
	    tabcour <- pvalues[pvalues$ind %in% contrast[i],]
	    temp <- ggplot(tabcour, aes(values))+geom_histogram(binwidth=width, color="#56B4E9", fill="white")+ labs(x="P-values")+ggtitle(unique(tabcour$ind))
	    p[[i]] <- temp
	    i <- i+1
	  }
	  # p<-ggplot(pvalues, aes(values))+geom_histogram(binwidth=width, color="#56B4E9", fill="white")+ labs(x="P-values")+facet_wrap(~ind, nrow=2)
	}

	priv.mcq.visualisation.graph(p, file=file)
	# if (is.null(file)){
	# 	print(p)
	# }else{
	# 	pdf(file, title=gsub('.pdf$','',file))
	# 	print(p)
	# 	dev.off()
	# }

}

pvalTitleMakerForContrast <- function(colname){

}
