

mcq.plot.rt.alignment <- function (directory=NULL, ylim=NULL, alpha=0.1, ncol=2, nrow=2, file=NULL) {
  ## adding colnames used in data.table or ggplot to silencing R CMD check global variable
  rt_MS2 <- deltaRt_MS2 <- post_mean_deltaRt_MS2 <- NULL
  ## checking format of argument
  error_messages = list()
  j=1
  if(!is.null(ylim)){
    if (!is.numeric(ylim[1])){
      error_messages[[j]] = "The 'ylim' argument must be a vector of two numeric."
      j=j+1
    }
    if(!is.numeric(ylim[2])){
      error_messages[[j]] = "The 'ylim' argument argument must be a vector of two numeric."
      j=j+1
    }
  }
  if(!is.numeric(alpha)){
    error_messages[[j]] = "The 'alpha' argument must be a numeric between 0 and 1."
    j=j+1
  }
  if(!is.character(directory)){
    error_messages[[j]] = "The 'directory' argument must be a character string."
    j=j+1
  }
  if(is.character(directory) & !file.exists(directory)){
    error_messages[[j]] = paste("The directory ", directory, " does not exist.", sep="")
    j=j+1
  }
  if(!is.character(file)&!is.null(file)){
    error_messages[[j]] = "The 'file' argument must be a character string."
    j=j+1
  }
  if (length(error_messages) != 0){
    stop(paste("Error on argument format:\n", paste(error_messages, collapse="\n"), sep=""))
  }


  nbgraph <- ncol*nrow
  j <- 1
  k <- 1
  sample = list.files(path=directory, pattern= "*.trace")
  p = list()
  cat(sample)

  while (j <= length(sample)){
    	temp = paste(directory, sample[j], sep="/")
      #cat(paste0("boucle while : ", temp, "\n"))
    	alignementall = fread(temp,sep="\t",header=T,dec=".", stringsAsFactors = F, data.table=FALSE)
    	colnames(alignementall)[4] <- "post_mean_deltaRt_MS2"
   	alignementall <- alignementall[,c("rt_MS2", "deltaRt_MS2", "post_mean_deltaRt_MS2")]
   	alignementall = na.omit(alignementall)
   	alignementall$titre=stringr::str_replace(sample[j], ".trace", "")
    if(length(sample)-j < nbgraph){
      nbgraph = length(sample)-j
    }
    #cat(paste0(j," : ", length(sample)-j, " ", nbgraph,"\n"))
    if(nbgraph!=0){
      for (i in c((j+1):(j+nbgraph-1))){
        ## ouverture du fichier .trace
        temp = paste(directory, sample[i], sep="/")
        #cat(paste0("boucle for : ", temp, "\n"))
        alignement = fread(temp,sep="\t",header=T,dec=".", stringsAsFactors = F, data.table=FALSE)
        colnames(alignement)[4] <- "post_mean_deltaRt_MS2"
        alignement <- alignement[,c("rt_MS2", "deltaRt_MS2", "post_mean_deltaRt_MS2")]
        alignement = na.omit(alignement)
        alignement$titre=stringr::str_replace(sample[i], ".trace", "")
        alignementall=rbind.data.frame(alignementall, alignement)
      }
      j <- j+nbgraph
    }else{
      j <- j+1
    }

    temp_graph = ggplot(alignementall, aes(x= rt_MS2, y=deltaRt_MS2))+geom_point(alpha = alpha)+geom_line(aes(x=rt_MS2, y=post_mean_deltaRt_MS2, color="red"))+facet_wrap(~titre, ncol=ncol, nrow=nrow, scales='free_y')
    if(!is.null(ylim)){
      temp_graph = temp_graph +ylim(ylim[1],ylim[2])
    }
    temp_graph <- temp_graph+labs(x="Retention time (s)", y="Delta of retention time (s)")+theme(plot.title =element_text(hjust = 0.5))+ theme(legend.position="bottom", legend.title = element_blank())+scale_color_discrete(labels = "correction factor")
    p[[k]] = temp_graph
    k <- k+1
  }


  priv.mcq.visualisation.graph(p, file)
  cat("\n")
}
