
mcq.plot.intensity.profile <- function (object, factorToColor=NULL, RTinterval=250, file=NULL, interactiveView=FALSE) {
	## adding colnames used in data.table or ggplot to silencing R CMD check global variable
	logarea <- msrun <- NULL
  # control object
	if ((is(object, "cmcq.pepq.masschroq")) || (is(object, "cmcq.pepq.skyline"))) {
		if(!priv.is.metadata.fill(object@metadata)){
			stop(paste("mcq.plot.intensity.profile needs that metadata are filled before running. \nPlease fill metadata before performing the plot (see ?mcq.read.metadata and ?mcq.add.metadata)"))
		}
	# }else if (class(object)=="cmcq.pepq.sds") {
	# 	unionobject <- mcq.union.fraction.sds(object)
	# 	object <- unionobject[[1]]
	}else{
		stop(paste("The class \"", class(object),"\" is not taken into account by mcq.plot.intensity.profile.\n",sep=""))
	}

  # control parameters : factorToColor
	if (!is.null(factorToColor)) {
		object@metadata@metadata$factorToColor=""
		if(length(factorToColor) !=1){
			for (color in factorToColor){
				if(color %in% colnames(object@metadata@metadata)) {
				object@metadata@metadata$factorToColor=paste(object@metadata@metadata$factorToColor, object@metadata@metadata[,color], sep=" ")
				}else{
					stop(paste("The factor ", color, " chosen for the 'factorToColor' argument is not described in your metadata\n", "please choose one in: ", paste(colnames(object@metadata@metadata),collapse=", "), "\n", sep=""))
				}
			}
		}else if(factorToColor == "msrun"){
			object@metadata@metadata$factorToColor= colnames(object@metadata@metadata)
		}else if(factorToColor%in% colnames(object@metadata@metadata)){
			object@metadata@metadata$factorToColor=paste(object@metadata@metadata$factorToColor, object@metadata@metadata[,factorToColor], sep=" ")
		}else{
			stop(paste("The factor ", factorToColor, " chosen for the 'factorToColor' argument is not described in your metadata\n", "please choose one in: ", paste(colnames(object@metadata@metadata),collapse=", "), "\n", sep=""))
		}
		

	}else{
		object@metadata@metadata$factorToColor="my_color"
	}

	if (interactiveView && !is.null(file)) {
		stop("Please choose only one display : interactiveVie w or file. \n ")
	}

	tab <- merge(object@peptides, object@metadata@metadata, by.x="msrun", by.y="row.names")
	mean.rt <- tapply(tab$rt, list(tab$peptiz), mean)
	mean.rt <- as.data.frame(mean.rt)
	mean.rt$peptiz <- rownames(mean.rt)

	j <- round(min(mean.rt$mean.rt, na.rm=TRUE),0)
	RT <- j <- j-j%%RTinterval
	jmax <- ceiling(max(mean.rt$mean.rt, na.rm=TRUE))
	jmax <- jmax - jmax%%RTinterval + RTinterval
	nclass <- ceiling((jmax-j)/RTinterval)
	for (i in 1:nclass){
		mean.rt$class[mean.rt$mean.rt>j]=i
		j=j+RTinterval
		RT=c(RT, j)
	}

	indice <- match(tab$peptiz, mean.rt$peptiz)
	class <- mean.rt$class[indice]

	if (is.null(object@peptides$lognorm)) {
#~ 		mat <- reshape2::melt(tapply(tab$logarea, list(class, tab$msrun), median))
		#mat <- melt(tapply(tab$logarea, list(class, tab$msrun), median))
		mat = as.data.frame(tapply(tab$logarea, list(class, tab$msrun), median))
		mat$class = row.names(mat)
		mat = melt(as.data.table(mat), id.vars="class")
		colnames(mat) <- c("class","msrun","logarea")
		n <- match(mat$msrun,tab$msrun)
		mat$factorToColor <- tab[n,"factorToColor"]
		mat$class = as.numeric(mat$class)

		p <- ggplot(data=mat, aes(x=class,y=logarea, group=msrun, color=factorToColor)) + geom_line()  + scale_x_continuous("Retention time", breaks= c(0.5:(nclass+0.5)), labels = RT) + scale_y_continuous(name="Median of Log10(intensity)")

	}else{
#~ 		mat1 <- reshape2::melt(tapply(tab$logarea, list(class, tab$msrun), median))

		mat1 = as.data.frame(tapply(tab$logarea, list(class, tab$msrun), median))
		mat1$class = row.names(mat1)
		mat1 = melt(as.data.table(mat1), id.vars="class")
		colnames(mat1) <- c("class","msrun","q")
		mat1$norm="Before normalization"

#~ 		mat2 <- reshape2::melt(tapply(tab$lognorm, list(class, tab$msrun), median))
		# mat2 <- melt(tapply(tab$lognorm, list(class, tab$msrun), median))
		mat2 = as.data.frame(tapply(tab$lognorm, list(class, tab$msrun), median))
		mat2$class = row.names(mat2)
		mat2 = melt(as.data.table(mat2), id.vars="class")
		colnames(mat2) <- c("class","msrun","q")
		mat2$norm="After normalization"

		mat=rbind.data.frame(mat1, mat2)

		n <- match(mat$msrun,tab$msrun)
		mat$factorToColor <- tab[n,"factorToColor"]
		mat$norm=factor(mat$norm, c("Before normalization", "After normalization"))
		mat$class = as.numeric(mat$class)

		p <- ggplot(data=mat, aes(x=class,y=q, group=msrun, color=factorToColor)) + 	geom_line()  + scale_x_continuous("Retention time", breaks= c(0.5:(nclass+0.5)), labels = RT) + scale_y_continuous(name="Median of Log10(intensity)")+facet_wrap(~norm)

	}

	if(is.null(factorToColor)){
		p <- p+theme(plot.title = element_text(hjust=0.5),legend.position = "none",axis.text.x = element_text(angle=90))
	}else{
		p <- p+theme(plot.title = element_text(hjust=0.5), legend.position = "bottom", axis.text.x = element_text(angle=90), legend.title = element_blank())
	}

	if (!is.null(file)) {
		pdf(file, title=gsub('.pdf$','',file), width=15)
		print(p)
		msg <- dev.off()
		cat("The pdf file  has been exported\n")
	}else if(interactiveView){
		p1 <- plotly::ggplotly(p)
		return(p1)
	}else{
		print(p)
	}

}
