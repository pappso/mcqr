###########
## Function to plot rt variability if peak
###########

mcq.plot.delta.rt <- function (object, minLimit = NULL,  maxLimit = NULL, width=5, adjust=1, byFraction = FALSE, ref=NULL, file=NULL) {

  fraction <- diff.rt <- NULL
  ### check argument format
  error_messages = list()
  j=1
  if (!is.numeric(minLimit)&!is.null(minLimit)){
    error_messages[[j]] = "The 'minLimit' argument must be numeric."
    j=j+1
  }
  if (!is.numeric(maxLimit)&!is.null(maxLimit)){
    error_messages[[j]] = "The 'maxLimit' argument must be numeric."
    j=j+1
  }
  if (!is.numeric(width)){
    error_messages[[j]] = "The 'width' argument must be numeric."
    j=j+1
  }
  if (!is.logical(byFraction)){
    error_messages[[j]] = "The 'byFraction' argument must be a boolean."
    j=j+1
  }
  if(!is.character(file)&!is.null(file)){
    error_messages[[j]] = "The 'file' argument must be a character string."
    j=j+1
  }
  if(!is.character(ref)){
    error_messages[[j]] = "The 'ref' argument must be a character string."
    j=j+1
  }
  if(!ref %in% unique(object@peptides$label)){
    lablevel=paste(unique(object@peptides$label), collapse=", ")
    error_messages[[j]] = paste("The label ", ref, " is not present in your data. Please select one level of labeling among the following: ", lablevel, "\n", sep="")
    j=j+1
  }
  if(byFraction & !object@is_fraction){
    error_messages[[j]] = "There is no fraction information in your input object. Please change the value of the 'byFraction' argument to FALSE. \n"
  }
  if(!is(object,"cmcq.pepq.masschroq.labeling")){
    error_messages[[j]] = paste("The object of class", class(object), " is not taken into account by 'mcq.plot.delta.rt'. \n", sep="")
  }
  if (length(error_messages) != 0){
    stop(paste("Error on argument format:\n", paste(error_messages, collapse="\n"), sep=""))
  }

### compute diff rt
  cat("preparing data\n")
  peptides = as.data.table(object@peptides)
  metadata = object@metadata@metadata
  metadata$msrun_label = rownames(metadata)
  metadata = as.data.table(metadata)
  peptides = merge(peptides, metadata)
  list_df = split(peptides, by=eval("label"))
  reftab = list_df[[ref]]
  reftab  = reftab [,c("msrun", "rt", "peptiz")]
  reftab $msrun_peptiz = paste(reftab$msrun, reftab$peptiz, sep="_")
  reftab  = reftab [,c("msrun_peptiz","rt")]
  colnames(reftab ) = c("msrun_peptiz", "rt.ref")
  list_df[[ref]]<- NULL

  for(i in 1:length(list_df)){
    df = list_df[[i]]
    if (object@is_fraction){
      df = df[,c("msrun", "rt", "peptiz", "fraction")]
    }else{
      df = df[,c("msrun", "rt", "peptiz")]
    }
    df$msrun_peptiz = paste(df$msrun, df$peptiz, sep="_")
    df = merge(df, reftab , by="msrun_peptiz")
    df$diff.rt = df$rt-df$rt.ref
    list_df[[i]] = df
  }

  if(!is.null(minLimit)){
    for(i in 1:length(list_df)){
      list_df[[i]] = list_df[[i]][list_df[[i]]$diff.rt > minLimit,]
    }
  }
  if(!is.null(maxLimit)){
    for(i in 1:length(list_df)){
      list_df[[i]] = list_df[[i]][list_df[[i]]$diff.rt < maxLimit,]
    }
  }

### prepare the graphs
  cat("preparing graph\n")
  p = list()

  for(i in 1:length(list_df)){
    rt_diff = list_df[[i]]
    if(!byFraction){
      temp <- ggplot(rt_diff, aes(x=diff.rt, color="red", fill="red"))+geom_histogram(binwidth = width, alpha=0.6) + theme(legend.position="none")
      temp <- temp+xlab("Delta of retention time") + labs(title=paste("Distribution of the DeltaRT between ", ref, " and ", names(list_df[i]), sep=""), subtitle=paste(" Median = ",round(median(rt_diff$diff.rt), 1), sep=""))+theme(plot.title =element_text(hjust = 0.5), plot.subtitle = element_text(hjust = 0.5))+guides(color="none")
      p[[i]] <- temp
    }else{
      temp <- ggplot(rt_diff, aes(x=diff.rt, color=fraction))+geom_density(alpha=0.6, adjust = adjust)+theme(legend.title = element_blank())
      temp <- temp+xlab("Delta of retention time") + labs(title=paste("Distribution of the DeltaRT between ", ref, " and ", names(list_df[i]), sep=""), subtitle=paste(" Median = ",round(median(rt_diff$diff.rt), 1), sep=""))+theme(plot.title =element_text(hjust = 0.5), plot.subtitle = element_text(hjust = 0.5))
      p[[i]] <- temp
    }
  }
  priv.mcq.visualisation.graph(p, file)
}
