
mcq.plot.intensity.distribution<- function (object, width=0.1, adjust=1, byTrack=FALSE, overlay=FALSE, file=NULL) {
## adding colnames used in data.table or ggplot to silencing R CMD check global variable
	logarea <- NULL

## check arguments
	error_messages <- list()
	j=1
	if (!is.numeric(width)){
		error_messages[[j]] <- "The 'width' argument must be numeric."
		j=j+1
	}
	if(!is.numeric(adjust) | adjust <=0 ){
	  error_messages[[j]] = "The 'adjust' argument must be numeric and higher than 0."
	  j=j+1
	}
	if(!is.character(file)&!is.null(file)){
		error_messages[[j]] = "The 'file' argument must be a character string."
		j=j+1
	}
	if(!is.logical(byTrack)){
	  error_messages[[j]] = "The 'byTrack' argument must be a boolean."
	  j=j+1
	}
	if(!is.logical(overlay)){
	  error_messages[[j]] = "The 'overlay' argument must be a boolean."
	  j=j+1
	}
	if (!class(object) %in% c("cmcq.pepq.masschroq","cmcq.pepq.masschroq.labeling")) {
 		stop(paste("The object of class ", class(object), " is not taken into account by mcq.plot.intensity.distribution.\n", sep=""))
	}

	## check for cmcq.pepq.masschroq
	if(is(object, "cmcq.pepq.masschroq")){
		if(byTrack & !priv.mcq.is.object.with.fraction(object)){
			error_messages[[j]] = "The argument \"byTrack\" is not used for data without fractionation"
			j=j+1
		}
	}
	## check for cmcq.pepq.masschroq.labeling
	if(is(object, "cmcq.pepq.masschroq.labeling") & !object@is_fraction){
		if(byTrack){
			error_messages[[j]] = "The argument \"byTrack\" is not used for data without fractionation"
			j=j+1
		}
	}

	if (length(error_messages) != 0){
		stop(paste("Error:\n", paste(error_messages, collapse="\n"), sep=""))
	}
	if (length(error_messages) != 0){
 		stop(paste("Error on argument format:\n", paste(error_messages, collapse="\n"), sep=""))
	}

## plot

	plot_list=list()
	if (!object@is_fraction){
		if(!overlay){
			i <- 1
			msrun <- unique(object@peptides$msrun)
			msrun=msrun[order(msrun)]
			while (i <= length(msrun)){
				tabcour <- object@peptides[object@peptides$msrun %in% msrun[i],]
				p <- ggplot(tabcour, aes(x=logarea, color="red", fill="red")) + geom_histogram(binwidth=width, alpha=0.6)+ labs(y="Number of peptides-mz", x="Log10(intensity)", title=paste("Distribution of log-transformed peptide-mz intensity for ", msrun[i], sep=""))+ theme(plot.title =element_text(hjust = 0.5), legend.position="none")
				plot_list[[i]] <- p
				i <- i+1
			}
		}else{
			## adding a switch based on the number of msrun to plotly if two many msrun to be able to show legend if file is null (number undetermined need test)
			## if file is specified the legend was removed
			if(is.null(file)){
				if(length(unique(object@peptides$msrun) > 6)){
					p <- ggplot(object@peptides, aes(x=logarea, color=msrun)) + geom_density(alpha=0.6)+ labs(y="Density", x="Log10(intensity)", title="Distribution of log-transformed peptide-mz intensity" , show.legend = FALSE)+theme(plot.title =element_text(hjust = 0.5))
					p1 <- plotly::ggplotly(p)
					return(p1)
				}else{
					p <- ggplot(object@peptides, aes(x=logarea, color=msrun)) + geom_density(alpha=0.6)+ labs(y="Density", x="Log10(intensity)", title="Distribution of log-transformed peptide-mz intensity")+theme(plot.title =element_text(hjust = 0.5), legend.title = element_blank())
					plot_list[[1]] <- p
				}
			}else{
				if(length(unique(object@peptides$msrun) > 6)){
					p <- ggplot(object@peptides, aes(x=logarea, color=msrun)) + geom_density(alpha=0.6)+ labs(y="Density", x="Log10(intensity)", title="Distribution of log-transformed peptide-mz intensity" , show.legend = FALSE)+theme(plot.title =element_text(hjust = 0.5))
					plot_list[[1]] <- p
				}else{
					p <- ggplot(object@peptides, aes(x=logarea, color=msrun)) + geom_density(alpha=0.6)+ labs(y="Density", x="Log10(intensity)", title="Distribution of log-transformed peptide-mz intensity")+theme(plot.title =element_text(hjust = 0.5), legend.title = element_blank())
					plot_list[[1]] <- p
				}
			}

			
		}
	}else{
		if(!byTrack){
			if(!overlay){
				i <- 1
				msrun <- unique(object@peptides$msrun)
				msrun=msrun[order(msrun)]
				while (i <= length(msrun)){
					tabcour <- object@peptides[object@peptides$msrun %in% msrun[i],]
					p <- ggplot(tabcour, aes(x=logarea, color="red", fill="red")) + geom_histogram(binwidth=width, alpha=0.6)+ labs(y="Number of peptides-mz", x="Log10(intensity)", title=paste("Distribution of log-transformed peptide-mz intensity for ", msrun[i], sep=""))+ theme(legend.position="none", plot.title =element_text(hjust = 0.5))
				plot_list[[i]] <- p
				i <- i+1
				}
			}else{
				if (length(unique(object@peptides$msrun))<21){
					p <- ggplot(object@peptides, aes(x=logarea, color=msrun)) + geom_density(alpha=0.6)+ labs(y="Density", x="Log10(intensity)", title="Distribution of log-transformed peptide-mz intensity")+theme(plot.title =element_text(hjust = 0.5), legend.title = element_blank(), legend.position="bottom")
					plot_list[[1]] <- p
				}else{
					p <- ggplot(object@peptides, aes(x=logarea, color=msrun)) + geom_density(alpha=0.6)+ labs(y="Density", x="Log10(intensity)", title="Distribution of log-transformed peptide-mz intensity")+theme(plot.title =element_text(hjust = 0.5), legend.title = element_blank(), legend.position="bottom", legend.text=element_text(size=6), legend.key.size=unit(0.1, 'cm'))
					plot_list[[1]] <- p				
				}
			}
		}else{
			peptides=object@peptides
			meta=object@metadata@metadata
			if(is(object, "cmcq.pepq.masschroq")){
				meta$msrun=rownames(meta)
				peptides=merge(peptides, meta, by="msrun")
			}else if (is(object,"cmcq.pepq.masschroq.labeling")){
				meta$msrun_label=rownames(meta)
				peptides=merge(peptides, meta, by="msrun_label")
			}
			if(!overlay){
				i <- 1
				track <- unique(peptides[,c("track", "track_order")])
				track=track$track[order(track$track_order)]
				while (i <= length(track)){
					tabcour <- peptides[peptides$track %in% track[i],]
					p <- ggplot(tabcour, aes(x=logarea, color="red", fill="red")) + geom_histogram(binwidth=width, alpha=0.6)+ labs(y="Number of peptides-mz", x="Log10(intensity)", title=paste("Distribution of log-transformed peptide-mz intensity for track ", track[i], sep=""))+ theme(legend.position="none", plot.title =element_text(hjust = 0.5))
				plot_list[[i]] <- p
				i <- i+1
				}
			}else{

				p <- ggplot(peptides, aes(x=logarea, color=track)) + geom_density(alpha=0.6, adjust=adjust)+ labs(y="Density", x="Log10(intensity)", title="Distribution of log-transformed peptide-mz intensity")+theme(plot.title =element_text(hjust = 0.5), legend.title = element_blank())
				plot_list[[1]] <- p
			}
		}
	}
	priv.mcq.visualisation.graph(plot_list, file)
}
