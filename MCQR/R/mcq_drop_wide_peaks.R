
mcq.drop.wide.peaks <- function (object, cutoff=100, byFraction=FALSE, verbose=TRUE) {
	## workaround for diann data to forbid suage of the function
	
	if(is(object, "cmcq.pepq.masschroq")){
		test_rt_column <- object@peptides$rtend - object@peptides$rtbegin
		if(length(test_rt_column) == 1){
			if(unique(test_rt_column) == 0){
				stop("mcq.drop.wide.peaks is not compatible with DIANN data\n")
			}
		}
	}

	## adding colnames used in data.table or ggplot to silencing R CMD check global variable
  peptiz <- peptide <- msrun <- protein <- NULL
	if (class(object) %in% c("cmcq.pepq.masschroq", "cmcq.pepq.masschroq.labeling")) {
	  peptides = as.data.table(object@peptides)
	  peptides$peak_width = peptides$rtend-peptides$rtbegin
	  if(object@is_fraction & byFraction){
	    peak_width = peptides[,list(maxpeakwidth = max(peak_width)), by=c("peptiz", "group")]
	    good.pep = peak_width[peak_width$maxpeakwidth < cutoff, c("peptiz", "group")]
	  }else{
	    peak_width = peptides[,list(maxpeakwidth = max(peak_width)), by=c("peptiz")]
	    good.pep = peak_width[peak_width$maxpeakwidth < cutoff, c("peptiz")]
	    # good_pep$group == fractiona1
	  }
		# bad.peaks <-(object@peptides[,'rtend'] - object@peptides[,'rtbegin']) > cutoff
		# bad.pep <- unique(object@peptides$peptiz[bad.peaks])
	}
	else if (is(object, "cmcq.pepq.skyline")) {
		bad.peaks <-(object@peptides[,'rtend'] - object@peptides[,'rtbegin']) > cutoff
		bad.pep <- unique(object@peptides$peptide[bad.peaks])
	}else {
		stop(paste("The class of the input object (",class(object),") is not taken into account by mcq.drop.wide.peaks."))
	}

	if (is(object, "cmcq.pepq.masschroq") | is(object, "cmcq.pepq.masschroq.labeling")) {
	  if(byFraction & object@is_fraction){
	    newobject <- priv.select.peptiz.fraction(object, good.pep)
	    # peptides <- merge(peptides, good.pep, by=c("peptiz", "group"))
	    # unique_peptiz <- unique(peptides[,c("peptide", "group")])
	    # colnames(unique_peptiz) <- c("peptide", "fraction")
	    # proteins <- as.data.table(object@proteins)
	    # proteins <- merge(proteins, unique_peptiz, by=c("peptide", "fraction"))
	    # msrun_keep <- unique(peptides$msrun)
	    # if(class(object) == "cmcq.pepq.masschroq"){
	    #   if(length(msrun_keep) != length(object@metadata@injections$msrun)){
	    #     injections <- object@metadata@injections
	    #     injections <- injections[which(injections$msrun %in% msrun_keep),]
	    #     metadata <- object@metadata@metadata
	    #     if(dim(metadata)[1] != 0){
	    #       metadata <- metadata[which(rownames(metadata) %in% msrun_keep),]
	    #     }
	    #     newmetadata <- new ("cmcq.metadata", injections=injections, metadata= metadata)
	    #   }else{
	    #     newmetadata <- object@metadata
	    #   }
	    #   newobject <- new("cmcq.pepq.masschroq", peptides = peptides	, proteins = proteins, metadata = newmetadata, is_imputed=object@is_imputed, is_fraction=object@is_fraction)
	    # }else if(class(object) == "cmcq.pepq.masschroq.labeling"){
	    #   if(length(msrun_keep) != length(object@metadata@injections$msrun)){
	    #     metadata <- object@metadata@metadata
	    #     injections <- object@metadata@injections
	    #     injections <- injections[which(injections$msrun %in% msrun_keep),]
	    #     msrun_label_keep = rownames(injections)
	    #     if(dim(metadata)[1] != 0){
	    #       metadata <- metadata[which(rownames(metadata) %in% msrun_label_keep),]
	    #     }
	    #     newmetadata <- new ("cmcq.metadata.labeling", injections=injections, metadata= metadata)
	    #   }else{
	    #     newmetadata <- object@metadata
	    #   }
	    #   newobject <- new("cmcq.pepq.masschroq.labeling", peptides = peptides	, proteins = proteins, metadata = newmetadata, is_imputed=object@is_imputed, is_fraction=object@is_fraction)
	    # }
	  }else{
	    # newobject <-  mcq.drop(object,factor="peptiz",levels=bad.pep$peptiz)
	    newobject <- priv.select.peptiz(object, good.pep)
	    # peptides <- merge(peptides, good.pep, by="peptiz")
	    # unique_peptiz <- unique(peptides$peptide)
	    # proteins <- as.data.table(object@proteins)
	    # proteins <- proteins[which(proteins$peptide %in% unique_peptiz)]
	    # msrun_keep <- unique(peptides$msrun)
	    # if(class(object) == "cmcq.pepq.masschroq"){
	    #   if(length(msrun_keep) != length(object@metadata@injections$msrun)){
	    #     metadata <- object@metadata@metadata
	    #     injections <- object@metadata@injections
	    #     injections <- injections[which(injections$msrun %in% msrun_keep),]
	    #     if(dim(metadata)[1] != 0){
	    #       metadata <- metadata[which(rownames(metadata) %in% msrun_keep),]
	    #     }
	    #     newmetadata <- new ("cmcq.metadata", injections=injections, metadata= metadata)
	    #   }else{
	    #     newmetadata <- object@metadata
	    #   }
	    #   newobject <- new("cmcq.pepq.masschroq", peptides = peptides	, proteins = proteins, metadata = newmetadata, is_imputed=object@is_imputed, is_fraction=object@is_fraction)
	    # }else if(class(object) == "cmcq.pepq.masschroq.labeling"){
	    #   if(length(msrun_keep) != length(object@metadata@injections$msrun)){
	    #     metadata <- object@metadata@metadata
	    #     injections <- object@metadata@injections
	    #     injections <- injections[which(injections$msrun %in% msrun_keep),]
	    #     msrun_label_keep = rownames(injections)
	    #     if(dim(metadata)[1] != 0){
	    #       metadata <- metadata[which(rownames(metadata) %in% msrun_label_keep),]
	    #     }
	    #     newmetadata <- new ("cmcq.metadata.labeling", injections=injections, metadata= metadata)
	    #   }else{
	    #     newmetadata <- object@metadata
	    #   }
	    #   newobject <- new("cmcq.pepq.masschroq.labeling", peptides = peptides	, proteins = proteins, metadata = newmetadata, is_imputed=object@is_imputed, is_fraction=object@is_fraction)
	    # }
	  }
	}else{
	  ### let it for others object (cmcq.pepq.skyline)
	  newobject <- mcq.drop(object,factor="peptide",levels=bad.pep)
	}
	# 	newobject <- mcq.drop(object,factor="peptiz",levels=bad.pep)
	# 	if (class(object) =="cmcq.pepq.sds") {
	# 		sink()
	# 	}
	# }else{
	# 	newobject <- mcq.drop(object,factor="peptide",levels=bad.pep)
	# }

	cat (paste("Info : ",length(unique(object@peptides$peptiz))-length(unique(newobject@peptides$peptiz)), " peptides-mz removed because of chromatographic peaks wider than ", cutoff, " seconds in at least one injection\n", sep=""))

	if (class(newobject) %in% c("cmcq.pepq.masschroq", "cmcq.pepq.skyline", "cmcq.pepq.masschroq.labeling")) {

		nbquanti.before <- nrow(object@peptides)
		nbpeptides.before <- length(unique(object@peptides$peptide))
		nbpeptiz.before <- length(unique(object@peptides$peptiz))
		nbprot.before <- length(unique(object@proteins$protein))
		nbquanti.after <- nrow(newobject@peptides)
		nbpeptides.after <- length(unique(newobject@peptides$peptide))
		nbpeptiz.after <- length(unique(newobject@peptides$peptiz))
		nbprot.after <- length(unique(newobject@proteins$protein))
		cat (paste("       corresponding to ",(nbpeptides.before - nbpeptides.after), " peptides, and ",(nbprot.before - nbprot.after), " proteins.\n", sep=""))
		if(verbose){
		  if ((class(newobject) %in% c("cmcq.pepq.masschroq","cmcq.pepq.masschroq.labeling")) & newobject@is_fraction){
		    npep_before = as.data.table(object@peptides)[,list(npepz_before = length(unique(peptiz)), npep_before = length(unique(peptide)), nmsrun_before = length(unique(msrun))), by="group"]
		    npep_after = as.data.table(newobject@peptides)[,list(npepz_after = length(unique(peptiz)), npep_after = length(unique(peptide)), nmsrun_after = length(unique(msrun))), by="group"]
		    bilan_pep = merge(npep_after, npep_before, by="group")
		    bilan_pep$nb.peptides.charge.lost = bilan_pep$npepz_before-bilan_pep$npepz_after
		    bilan_pep$nb.peptides.lost = bilan_pep$npep_before-bilan_pep$npep_after
		    bilan_pep$nb.msruns.lost = bilan_pep$nmsrun_before-bilan_pep$nmsrun_after
		    bilan_pep = bilan_pep[,c("group", "nb.peptides.charge.lost", "nb.peptides.lost", "nb.msruns.lost")]
		    colnames(bilan_pep) <- c("fraction", "nb of peptides-mz removed", "nb of peptides removed", "nb of msruns removed")
		    nprot_before = as.data.table(object@proteins)[,list(nprot_before = length(unique(protein))), by="fraction"]
		    nprot_after = as.data.table(newobject@proteins)[,list(nprot_after = length(unique(protein))), by="fraction"]
		    bilan_prot = merge(nprot_after, nprot_before, by="fraction")
		    bilan_prot$"nb of proteins removed" = bilan_prot$nprot_before-bilan_prot$nprot_after
		    bilan_prot = bilan_prot[,c("fraction", "nb of proteins removed")]
		    bilan.fraction = merge(bilan_pep, bilan_prot, by="fraction")

		    cat("Details by fractions :\n")
		    print(bilan.fraction)
		  }
		}
	}
	return (newobject)
}
