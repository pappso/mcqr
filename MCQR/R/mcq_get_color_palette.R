### method to check choosen color palette for the session

mcq.get.color.palette <- function(){
  cat(paste0("Current color palette is ", MCQRenv$mcqPalette, "\n"))
  if (RColorBrewer::brewer.pal.info$colorblind[rownames(RColorBrewer::brewer.pal.info) == MCQRenv$mcqPalette] == TRUE){
    cat("This palette is color blind friendly\n")
  }
}
