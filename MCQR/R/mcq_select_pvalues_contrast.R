## switch to S4 method for simplicity and portability
# definition of generic function
setGeneric("mcq.select.pvalues.contrast", function(object, padjust=TRUE, alpha=0.05, contrastName=NULL){})

# definition for all others object
# setMethod("mcq.select.pvalues.contrast", signature(object="ANY", padjust="ANY", alpha="ANY", contrastName="ANY"), function(object, padjust,  alpha, contrastName){
#   stop(paste0("Your data of type ", class(object), " is not supported by mcq.select.pvalues.contrast method.\n"))
# })

# definition for cmcq.anova.contrast without contrastName defined
# setMethod("mcq.select.pvalues.contrast", signature(object="cmcq.anova.contrast", padjust="ANY", alpha="ANY", contrastName="ANY"), function(object, padjust,  alpha, contrastName){
#   stop(paste0("The 'contrastName' argument is mandatory. Please select one from : \n\t", paste(colnames(object@pvalue.lm.adjust), collapse="\n\t"), "\n"))
# })

## definition for cmcq.anova.contrast
setMethod("mcq.select.pvalues.contrast", signature(object="cmcq.anova.contrast", padjust="ANY", alpha="ANY", contrastName="ANY"), function(object, padjust,  alpha, contrastName){
    ## controle argument
    error_message <- c()
    if(!is.null(padjust)){
        if(!is.logical(padjust)){
            error_message = c(error_message, "'padjust' argument must be a logical, please select between TRUE or FALSE.")
        }
    }
    if(!is.null(alpha)){
        if(!is.numeric(alpha)){
            error_message = c(error_message, "'alpha' argument must be a strictly positive numeric lower than 1.")
        }else if(!((alpha>0) & (alpha<1))){
            error_message = c(error_message, "'alpha' argument must be a strictly positive numeric lower than 1.")
        }
    }
    contrastNameList <- colnames(object@pvalue.lm.adjust)
    
    if(!is.null(contrastName)){
		if (!(contrastName %in% contrastNameList)){
			error_message = c(error_message, paste0("The 'contrastName' is not present in the object. Please select one from : \n\t", paste(contrastNameList, collapse="\n\t"), "\n"))
		}
	}else{
		error_message = c(error_message,paste0("The 'contrastName' argument is mandatory. Please select one from : \n\t", paste(colnames(object@pvalue.lm.adjust), collapse="\n\t"), "\n"))
	}
    if(length(error_message) > 0){
        stop(paste(error_message, collapse="\n"))
    }

    ## selection of variable valid on shapiro aznd levene test
    validableVariable = rownames(object@pvalue.tests)[object@pvalue.tests[,7] ==1]

    ## get relevanty matrice based on padjust argument
    if(padjust){
        testMatrice = object@pvalue.lm.adjust
        p = "adjusted pvalue"
    }else{
        testMatrice = object@pvalue.lm
        p = "pvalue"
    }

    ## select only variable valid from levenee and shapiro test
    testMatrice <- testMatrice[rownames(testMatrice) %in% validableVariable,]
    NbProt = dim(testMatrice)[1]
    ## select variable valid for the relevant contrastName and alpha value
    validVariable <- rownames(testMatrice)[testMatrice[,contrastName] < alpha]
    newNbProt = length(validVariable)
    if(extends(object@paramlist$from, "cmcq.generic.protq")){
        names(validVariable) = validVariable
        paramlist = list()
        paramlist$from = "cmcq.anova.contrast"
        paramlist$contrastName = contrastName
        paramlist$alpha = alpha
        paramlist$padjust = padjust
        newObject = new("cmcq.protlist", proteins= validVariable,  paramlist=paramlist)
        variableName = "proteins"
    }

    cat (paste("Info : The ", contrastName, " contrast is significant for ", newNbProt," ", variableName, " out of ", NbProt, " (",p," < ",alpha,").\n", sep=""))

    return(newObject)

})

