## function to get peplist object from data containing peptide information (cmcq.pepq family and cmcq.pepq.labeling family)

# definition of signature 

setGeneric("mcq.get.peplist", function(object){
})


# definition of function for unknown object

## definition for all others object
setMethod("mcq.get.peplist", signature(object="ANY"), function(object){
	stop(paste0("Your data of type ", class(object), " is not supported by mcq.get.peplist method.\n"))
})


## definition for cmcq.generic.pepq

setMethod("mcq.get.peplist", signature(object="cmcq.generic.pepq"), function(object){
	## check if proForma column is fill or not
	if("ProForma" %in% colnames(object@proteins)){
        # retrieve column peptide and peptiz from peptides table of object
        peplist <- unique(object@peptides[,c("peptide", "peptiz")])
        # retrieve column peptide and ProForma column from proteins table of object
        pepProForma <- unique(object@protiens[,c("peptide", "ProForma")])
        # merging the two table to associate peptiz to ProForma column
        peplist <- merge(peplist, pepProForma, by="peptide")
        # construction of finazl list with ProForma column as data and peptiz as names
        pepVector <- as.character(peplist$ProForma)
        names(pepVector) <- as.character(peplist$peptiz)
	}else{
        # construction of ProForma column (first just concatenation of mod and sequence information need function to make it properly)
        peplist <-  unique(object@peptides[,c("peptide", "peptiz", "sequence", "mods")])
        peplist$ProForma <- paste0(peplist$sequence, "_", peplist$mods)
        pepVector <- as.character(peplist$ProForma)
        names(pepVector) <- as.character(peplist$peptiz)
	}
    newObject <- new ("cmcq.peplist", peptides = pepVector)
	return (newObject)
})

## definition for cmcq.generic.pepq.labeling need test but in theory the same function than cmcq.generic.pepq would work

setMethod("mcq.get.peplist", signature(object="cmcq.generic.pepq.labeling"), function(object){
	## check if proForma column is fill or not
	if("ProForma" %in% colnames(object@proteins)){
        # retrieve column peptide and peptiz from peptides table of object
        peplist <- unique(object@peptides[,c("peptide", "peptiz")])
        # retrieve column peptide and ProForma column from proteins table of object
        pepProForma <- unique(object@proteins[,c("peptide", "ProForma")])
        # merging the two table to associate peptiz to ProForma column
        peplist <- merge(peplist, pepProForma, by="peptide")
        # construction of finazl list with ProForma column as data and peptiz as names
        pepVector <- as.character(peplist$ProForma)
        names(pepVector) <- as.character(peplist$peptiz)
	}else{
        # construction of ProForma column (first just concatenation of mod and sequence information need function to make it properly)
        peplist <-  unique(object@peptides[,c("peptide", "peptiz", "sequence", "mods")])
        peplist$ProForma <- paste0(peplist$sequence, "_", peplist$mods)
        pepVector <- as.character(peplist$ProForma)
        names(pepVector) <- as.character(peplist$peptiz)
	}
    newObject <- new ("cmcq.peplist", peptides = pepVector)
	return (newObject)
})

setMethod("mcq.get.peplist", signature(object="cmcq.tukey"), function(object){
        # retrieve column peptide and peptiz from peptides table of object
        peplist <- unique(object@proteins[,c("peptiz", "ProForma", "accession")])
        peplist$proformaaccession <- paste(peplist$ProForma, peplist$accession, sep="--")
        # construction of finazl list with ProForma column as data and peptiz as names
        pepVector <- as.character(peplist$proformaaccession)
        names(pepVector) <- as.character(peplist$peptiz)
    newObject <- new ("cmcq.peplist", peptides = pepVector)
	return (newObject)
})

